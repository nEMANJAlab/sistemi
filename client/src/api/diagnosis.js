import axios from 'axios'

export const makeDiagnosis = async(diagnosis) => {
    console.log(diagnosis)
    return await axios({
        url: `http://localhost:8080/diagnosis`,
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        data: { patientId: diagnosis.diagnosis.patientId,
                symptomsDTO: diagnosis.diagnosis.symptomsDTO,
                medicationsDTO: diagnosis.diagnosis.medicationsDTO
        }
    })
}

export const getReport = async() => {
    return await axios({
        url: `http://localhost:8080/hronicno`,
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}

export const getAddiction = async() => {
    return await axios({
        url: `http://localhost:8080/zavisnost`,
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}

export const fetchMedications = async() => {
    return await axios({
        url: `http://localhost:8080/medications`,
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}

export const deleteDiagnosis = async(id) => {
    return await axios({
        url: `http://localhost:8080/diagnosis/${id}`,
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}