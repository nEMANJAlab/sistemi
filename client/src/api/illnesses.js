import axios from 'axios';

export const fetchIllnesses = async () => {
    return await axios({
        url: `http://localhost:8080/illnesses`,
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}

export const fetchIllness = async (id) => {
    return await axios({
        url: `http://localhost:8080/illness/${id}`,
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}

export const fetchSymptomsByIllness = async (name) => {
    console.log(name)
    return await axios({
        url: `http://localhost:8080/illness/${name}/symptoms`,
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}

export const createIllness = async (body) => {
    return await axios({
        urll: `http://localhost:8080/illness`,
        method: "POST",
        headers: {
            "Content-Type":"application/json",
            "Accept": "application/json"
        },
        data: {
            illness: body
        }
    })
}

export const deleteIllness = async (id) => {
    return await axios({
        url: `http://localhost:8080/illness/${id}`,
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}