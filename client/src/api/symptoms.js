import axios from 'axios';

export const fetchSymptoms = async () => {
    return await axios({
        url: `http://localhost:8080/symptoms`,
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}

export const fetchSymptom = async (id) => {
    return await axios({
        url: `http://localhost:8080/symptom/${id}`,
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}

export const createSymptom = async (body) => {
    console.log(body)
    console.log('create symptom ')
    return await axios({
        url: `http://localhost:8080/symptom`,
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        data: { body }
    })
}

export const deleteSymptom = async (id) => {
    return await axios({
        url: `http://localhost:8080/symptom/${id}`,
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}