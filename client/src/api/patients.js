import axios from 'axios';


export const fetchPatient = async (id) => {
    return await axios({
        url: `http://localhost:8080/patient/${id}`,
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}

export const fetchPatients = async () => {
    return await axios({
        url: `http://localhost:8080/patients`,
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}

export const createPatient = async (body) => {
    console.log(body.allergiesDTO)
    return await axios({
        url: `http://localhost:8080/patient`,
        method: "POST",
        headers: {
            "Content-Type":"application/json",
            "Accept":"application/json"
        },
        data: {
            name: body.name, age: body.age, surname: body.surname, allergies: body.allergiesDTO
        }
    })
}

export const deletePatient = async (id) => {
    return await axios({
        url: `http://localhost:8080/patients/${id}`,
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}