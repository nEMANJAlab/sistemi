import React, { Component } from 'react';
import NavbarJS from './containers/NavbarJS';
import store from './store';
import { Provider } from 'react-redux';
import Particles from 'react-particles-js';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Symptoms from '../src/containers/symptoms';
import Illnesses from '../src/containers/illnesses';
import Patient from '../src/containers/patient';
import PatientFile from '../src/containers/patientFile';
import Patients from '../src/containers/patients';
import Home from '../src/containers/home';
import Diagnosis from '../src/containers/diagnosis'
import Report from '../src/containers/report'

class App extends Component {
  
  render() {
    //<div className="heart">❤♵☫☢ </div>
    return (
      <Provider store={store}>
      <Router>
          <div className="App" >
          <Particles params={{ particles: 
                { number: { value: 100, density: { enable: true, value_area: 800}}} }} 
                style={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                  }}/>
            <NavbarJS/>
            <Route path="/pacijenti" component={Patients} />
            <Route exact path="/" component={Home} />
            <Route exact path="/simptomi" component={Symptoms} />
            <Route exact path="/novkarton" component={PatientFile} />
            <Route exact path="/bolesti" component={Illnesses} />
            <Route exact path="/pacijent" component={Patient} />
            <Route path="/karton" component={Diagnosis} />
            <Route path="/izvestaj" component={Report} />
          </div>
          
          </Router>
      </Provider>
    );
  }
}

export default App;
