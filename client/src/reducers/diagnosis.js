import { MAKE_DIAGNOSIS, MAKE_DIAGNOSIS_SUCCESS, MAKE_DIAGNOSIS_FAILURE, FETCH_MEDICATIONS, FETCH_MEDICATIONS_FAILURE, FETCH_MEDICATIONS_SUCCESS, DELETE_DIAGNOSIS, DELETE_DIAGNOSIS_SUCCESS, DELETE_DIAGNOSIS_FAILURE, GET_REPORT, GET_REPORT_SUCCESS, GET_REPORT_FAILURE, GET_ADDICTION, GET_ADDICTION_SUCCESS, GET_ADDICTION_FAILURE } from '../actions/diagnosis';

export default (state = {diagnosis: null, report: [], addiction: [],  medications: [], created: false, loading: false, error: false}, action) => {
    switch(action.type){
        case MAKE_DIAGNOSIS:
            return {...state}
        case MAKE_DIAGNOSIS_SUCCESS:
            return {...state, created: true, diagnosis: action.payload.data}
        case MAKE_DIAGNOSIS_FAILURE:
            return {...state, created:false, error:true }
        case GET_REPORT:
            return {...state}
        case GET_REPORT_SUCCESS:
            return {...state, report: action.payload.data}
        case GET_REPORT_FAILURE:
            return {...state, error: true }
        case GET_ADDICTION:
            return {...state}
        case GET_ADDICTION_SUCCESS:
            return {...state, addiction: action.payload.data}
        case GET_ADDICTION_FAILURE:
            return {...state, error: true }
        case FETCH_MEDICATIONS:
            return {...state}
        case FETCH_MEDICATIONS_SUCCESS:
            return {...state, created: true,  medications: action.payload.data
            }
        case FETCH_MEDICATIONS_FAILURE:
            return {...state, created:false, error:true }
        case DELETE_DIAGNOSIS:
            return {...state}
        case DELETE_DIAGNOSIS_SUCCESS:
            return {...state, error: false
            }
        case DELETE_DIAGNOSIS_FAILURE:
            return {...state, error:true }
        default:
            return state;
    }

};