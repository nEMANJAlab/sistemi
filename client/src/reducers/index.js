import { combineReducers } from 'redux';
import illnesses from './illnesses';
import symptoms from './symptoms';
import patients from './patients';
import diagnosis from './diagnosis';

export default combineReducers({
    illnesses, symptoms, patients, diagnosis
});