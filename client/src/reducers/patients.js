import { FETCH_PATIENTS, FETCH_PATIENTS_SUCCESS, FETCH_PATIENTS_FAILURE,
    FETCH_PATIENT, FETCH_PATIENT_SUCCESS, FETCH_PATIENT_FAILURE, CREATE_PATIENT, CREATE_PATIENT_SUCCESS, CREATE_PATIENT_FAILURE} from '../actions/patients';

export default (state = {list:[], patient:null, error:false, loading:false, created:false}, action) => {
    switch(action.type){
        case CREATE_PATIENT:
            return{
                ...state
            }
        case CREATE_PATIENT_SUCCESS:
            return{ ...state,
            created: true
        }
        case CREATE_PATIENT_FAILURE:
            return{
                ...state, 
                error: true
            }
        case FETCH_PATIENT:
            return{
                ...state
            }
        case FETCH_PATIENT_SUCCESS:
            return {
                ...state,
                patient: action.payload.data
            }
        case FETCH_PATIENT_FAILURE:
        return {
            ...state,
            error:true,
            created:false
        }
        case FETCH_PATIENTS:
            return{
                ...state
            }
        case FETCH_PATIENTS_SUCCESS:
            return {
                ...state,
                list: action.payload.data
            }
        case FETCH_PATIENTS_FAILURE:
        return {
            ...state,
            error:true
        }
        default:
            return state;
    }

}