import { FETCH_ILLNESSES, FETCH_ILLNESSES_SUCCESS, 
    FETCH_ILLNESSES_FAILURE, CREATE_ILLNESS, 
    CREATE_ILLNESS_SUCCESS, CREATE_ILLNESS_FAILURE, 
    DELETE_ILLNESS, DELETE_ILLNESS_SUCCESS, DELETE_ILLNESS_FAILURE,
    FETCH_ILLNESS, FETCH_ILLNESS_FAILURE, FETCH_ILLNESS_SUCCESS, GET_SYMPTOMS_BY_ILLNESS, GET_SYMPTOMS_BY_ILLNESS_SUCCESS, GET_SYMPTOMS_BY_ILLNESS_FAILURE} 
    from '../actions/illnesses';

export default (state = {list: [], illnessSymptoms: [], created: false, loading: false, error: false}, action) => {
    switch(action.type){
        case GET_SYMPTOMS_BY_ILLNESS:
            return{...state}
        case GET_SYMPTOMS_BY_ILLNESS_SUCCESS:
            return {...state,
                illnessSymptoms: action.payload.data
            }
        case GET_SYMPTOMS_BY_ILLNESS_FAILURE:
            return{...state, error: true}
        case FETCH_ILLNESSES:
            return{...state}
        case FETCH_ILLNESSES_SUCCESS:
            return {...state,
                list: action.payload.data
            }
        case FETCH_ILLNESSES_FAILURE:
            return{...state, error: true}
        case FETCH_ILLNESS:
            return{...state}
        case FETCH_ILLNESS_SUCCESS:
            return {...state,
                list: [...state.list, action.payload.data]
            }
        case FETCH_ILLNESS_FAILURE:
            return{...state, error: true}
        case CREATE_ILLNESS:
            return{...state}
        case CREATE_ILLNESS_SUCCESS:
            return {...state,
                list: [...state.list, action.payload.data]
            }
        case CREATE_ILLNESS_FAILURE:
            return{...state, error: true}
        case DELETE_ILLNESS:
            return{...state}
        case DELETE_ILLNESS_SUCCESS:
            return {...state,
                list: action.payload.data
            }
        case DELETE_ILLNESS_FAILURE:
            return{...state, error: true}
        default:
            return state;
    }

};