import { FETCH_SYMPTOMS, FETCH_SYMPTOMS_SUCCESS, 
        FETCH_SYMPTOMS_FAILURE, CREATE_SYMPTOM, 
        CREATE_SYMPTOM_SUCCESS, CREATE_SYMPTOM_FAILURE, 
        DELETE_SYMPTOM, DELETE_SYMPTOM_SUCCESS, DELETE_SYMPTOM_FAILURE,
        FETCH_SYMPTOM, FETCH_SYMPTOM_FAILURE, FETCH_SYMPTOM_SUCCESS} 
        from '../actions/symptoms';

export default (state = {list: [], created: false, loading: false, error: false}, action) => {
    switch(action.type){
        case FETCH_SYMPTOMS:
            return{...state}
        case FETCH_SYMPTOMS_SUCCESS:
            return {...state,
                    list: action.payload.data
            }
        case FETCH_SYMPTOMS_FAILURE:
            return{...state, error: true}
        case FETCH_SYMPTOM:
            return{...state}
        case FETCH_SYMPTOM_SUCCESS:
            return {...state,
                    symptom: action.payload.data
            }
        case FETCH_SYMPTOM_FAILURE:
            return{...state, error: true}
        case CREATE_SYMPTOM:
            return{...state}
        case CREATE_SYMPTOM_SUCCESS:
            return{...state, 
                list: [...state.list, action.payload.data]}
        case CREATE_SYMPTOM_FAILURE:
                return{...state, error: true}
        case DELETE_SYMPTOM:
            return{...state}
        case DELETE_SYMPTOM_SUCCESS:
            return{...state, 
                list: action.payload.data}
        case DELETE_SYMPTOM_FAILURE:
                return{...state, error: true}
        default:
            return state;
    }

};