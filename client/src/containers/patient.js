import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom'
import { Card, CardImage, CardBody, CardTitle,
        CardText, Dropdown, DropdownToggle, 
        DropdownMenu, DropdownItem } from 'mdbreact';
import { makeDiagnosis, fetchMedications } from '../actions/diagnosis';
import { fetchPatients } from '../actions/patients';

class Patient extends Component {

    componentDidMount(){

        this.props.fetchMedications();
    }

    constructor(props) {
      super(props);
      this.state = {
        modal: false,
        symptomsDTO: {},
        patientId: this.props.patient.id,
        medicationsDTO: [],
        patientAllergies: this.props.patient.allergies
        
      };
    }

    toggle = () => {
      this.setState({
        modal: !this.state.modal,
        symptomsDTO: []
      });
    }
    
    getSymptom = (symptom) => {
      if(!this.state.symptomsDTO.filter(s => s.name === symptom).length > 0){
        this.setState( state => ({
          symptomsDTO: [...state.symptomsDTO, {"name":symptom}]
         }))
      }else{
        this.setState({
          symptomsDTO: this.state.symptomsDTO.filter(s => s.name !== symptom)
       })
      }
    }

    getMedication = (medication) => {
      this.setState( state => ({
        medicationsDTO: [...state.medicationsDTO, {"name":medication}]
      }))
    }

    makeDiag = () => {
      if(this.state.symptomsDTO.length !== 0){
        this.props.makeDiagnosis(this.state)
      }else{
       alert("Please pick a symptom/s")
      }
     
    }

    render() {
        var patient = this.props.patient;
        
        return (
        <div className="col">
        <br/>
        <Card className="h-100" cascade>
          <CardImage cascade tag="div">
              <div className="view gradient-card-header rgba-grey-light">
                  <h2 className="h2-responsive text-black">* {patient.name} {patient.surname} </h2>
                  <p className="black-text">Starost: {patient.age}</p>
              </div>
          </CardImage>
          <CardBody cascade>
                <CardTitle>Istorija</CardTitle>
                <div>
                    <div > 
                      
                      {
                        patient.diagnosisDTO.map((diagnosis, id) => {
                          
                          return(
                            <div key={id}>
                              {
                                diagnosis.illnessesDTO.map((illness, id) => {
                                  if(illness.name !== 'None'){
                                  return(
                                    <div key={id}>
                                      Bolest: {illness.name}
                                      <Dropdown>
                                          <DropdownToggle nav caret>Detalji</DropdownToggle>
                                          <DropdownMenu>
                                          {
                                            diagnosis.symptomsDTO.map((symptom, id) => {
                                              return(
                                                <div key={id}>
                                                <DropdownItem href=""><CardText>{symptom.name}</CardText></DropdownItem>
                                                </div>
                                              )
                                            })
                                          }
                                          <p style={{paddingLeft: "10px"}}> Terapija: </p>
                                          {
                                            diagnosis.therapyDTO.map((therapy, id) => {
                                              return(
                                                <div key={id}>
                                                {
                                                  therapy.medicationsDTO.map((medication, id) => {
                                                    return(
                                                      <div key={medication.id}>
                                                      <DropdownItem href=""><CardText>{medication.name}</CardText></DropdownItem>
                                                      </div>
                                                    )
                                                  })
                                                }
                                                </div>
                                              )
                                            })
                                          }
                                          </DropdownMenu>
                                      </Dropdown>
                                      
                                    </div>
                                  )}
                                })
                              }
                               
                            </div>
                          )
                        })
                      } 
                    </div>
                </div>
          </CardBody>

          <Link className="button" color="danger" to={`/karton/${patient.id}`}>Prikazi</Link>
        </Card>
        
      </div>

        )
    }
}

const mapStateToProps = state => ({
  medications: state.diagnosis.medications
})

const mapDispatchToProps = dispatch => bindActionCreators({ makeDiagnosis, fetchPatients, fetchMedications }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Patient);