import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchIllnesses } from '../actions/illnesses';
import { Table, TableHead, TableBody } from 'mdbreact';
import { Button } from 'mdbreact';
import { getSymptoms } from '../actions/illnesses';

class Illnesses extends Component {
    constructor(props) {
		super(props);
		this.state = {
            illness: ""
		}
    }

    componentDidMount(){
        this.props.fetchIllnesses();
    }

    handleChange = (e) => {
        this.setState({ illness: e.target.value})
    }

  render() {
    const { illnesses } = this.props;
    const { illnessSymptoms } = this.props;
    return (<div>
        <Table small>
        <TableHead color="blue-gradient" textWhite >
            <tr>
            <th>ID</th>
            <th>Illness</th>
            </tr>
        </TableHead>
        <TableBody>
        
        {
            illnesses.map((illness, id) => {
                return(<tr key={id}>
                    <td className="white-text">{illness.id}</td>
                    <td className="white-text">{illness.name}</td>
                    </tr>
                )
            })
        }
        </TableBody>
        </Table>
        <div style={{ paddingLeft: "50px"}}> 
            <form className="form-inline md-form mt-0">
                <input onChange={this.handleChange} className="form-control mr-sm-2 mb-0" type="text" placeholder="Unesi  bolest" aria-label="Search"/>
                <Button color="success" onClick={() => this.props.getSymptoms(this.state.illness)} > Prikazi</Button>
            </form>
        </div>
        {
            illnessSymptoms.map((symptom, id) => {
                return(
                    <div key={id} style={{ color: "white"}}> {symptom.name}</div>
                    
                )
            })
        }

      </div>
    )
  }
}

const mapStateToProps = state => ({
    illnesses: state.illnesses.list,
    illnessSymptoms: state.illnesses.illnessSymptoms
});

const mapDispatchToProps = dispatch => bindActionCreators({ fetchIllnesses, getSymptoms }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Illnesses);
