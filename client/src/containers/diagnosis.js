import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { makeDiagnosis, fetchMedications, deleteDiagnosis } from '../actions/diagnosis';
import { fetchPatients, fetchPatient } from '../actions/patients';
import { Button } from 'mdbreact';
import { fetchSymptoms } from '../actions/symptoms';
import Select from 'react-select';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {Link} from 'react-router-dom'

class diagnosis extends Component {
    constructor(props) {
		super(props);
		this.state = {
            patientId: this.props.location.pathname.slice(8),
            symptomsDTO: [],
            medicationsDTO: [],
            selectedSymptom: null,
            selectedMedication: null
		}
    }
    
    notify = (ingredient, medication) => {
        toast.error(`Pacijent je alergican na sastojak: ${ingredient} u leku: ${medication}`, {
            position: toast.POSITION.TOP_CENTER
        });
    };

    notifySymptoms = () => {
        toast(`Morate izabrati simptome.`, {
            position: toast.POSITION.TOP_CENTER
        });
    };

    addSymptom = (e) => {
        var symptoms = []
        e.forEach(element => {
            symptoms.push({name: element.value})
        });
        this.setState({ symptomsDTO: symptoms})
    }

    addMedication = (e) => {
        var medications = []
        this.props.medications.forEach(medication => {
            e.forEach(element => {
                if(element.value === medication.name){
                    medication.ingredients.forEach(ingredient => {
                        this.props.patient.allergies.forEach(allergy => {
                            if(ingredient.toLowerCase() === allergy.toLowerCase()){
                                this.notify(ingredient, medication.name)
                            }
                        });
                    });
                }
            });
        });
        
        e.forEach(element => {
            medications.push({name: element.value})
        });
        this.setState({ medicationsDTO: medications})
    }

    componentDidMount(){
        this.props.fetchPatient(this.props.location.pathname.slice(8))
        this.props.fetchSymptoms()
        this.props.fetchMedications()
        this.makeOptionsForSymptoms();
        this.makeOptionsForMedications();
    }

    componentWillReceiveProps(){
        this.makeOptionsForSymptoms();
        this.makeOptionsForMedications();
    }

    makeOptionsForSymptoms = () => {
        var sy = this.props.symptoms;
        var options = []
        sy.forEach(element => {
            options.push({ value: element.name, label: element.name })
        });
        this.setState({
            symptoms: options
        })
    }

    makeOptionsForMedications = () => {
        var sy = this.props.medications;
        var options = []
        sy.forEach(element => {
            options.push({ value: element.name, label: element.name })
        });
        this.setState({
            medications: options
        })
    }

    getSymptom = (symptom) => {
        if(!this.state.symptomsDTO.filter(s => s.name === symptom).length > 0){
          this.setState( state => ({
            symptomsDTO: [...state.symptomsDTO, {"name":symptom}]
           }))
        }else{
          this.setState({
            symptomsDTO: this.state.symptomsDTO.filter(s => s.name !== symptom)
         })
        }
    }

    makeDiag = () => {
        if(this.state.symptomsDTO.length !== 0){
          this.props.makeDiagnosis(this.state)
        }else{
         this.notifySymptoms()
        }
       
      }

    checkForDiagnosis = (diagnosis) => {
        if(diagnosis != null){
            return( <div><h2 className="diagnosis"> Najverovatnija bolest:  {diagnosis.illnessesDTO[0].name} </h2>
            <Button color="success" onClick={() => this.props.deleteDiagnosis(diagnosis.id)} >Odbaci dijagnozu</Button></div>)
        }
    }

    render() {
        const { selectedOption } = this.state;
        const { patient } = this.props;
        const { symptoms } = this.props;
        const { diagnosis } = this.props;
        if(patient === null || symptoms.length <= 0){
            return null;
        }else{
        return (
        <div style={{ backgroundColor: "rgba(255,255,255,0.15)", height: "100vh"}}>
                <h2 className="white-text font-weight-bold">Pacijent: {patient.name} {patient.surname}</h2>
                
                <span className="yellow-text font-weight-bold">Alergije:  {patient.allergies.map((allergy, id) => {
                    return(
                    <span key={id}>{allergy} </span>
                    )
                })}</span>
                <div className="post-content">
                    <p className="white-text">Izaberite simptome:</p>
                    <Select className="container" isMulti
                    value={selectedOption}
                    onChange={this.addSymptom}
                    options={this.state.symptoms}
                />
                <br/>
                <p className="white-text">Izaberite terapiju:</p>
                <Select className="container" isMulti
                    value={selectedOption}
                    onChange={this.addMedication}
                    options={this.state.medications}
                />
                </div>
                <br/><Link to='/pacijenti'>
                <Button color="danger" >Nazad</Button>
                </Link>
                <Button color="success" onClick={this.makeDiag} >Uradi dijagnozu</Button>
          <ToastContainer />
          {this.checkForDiagnosis(diagnosis)}
          
        </div>
         
        )}

       
  }
}

const mapStateToProps = state => ({
    medications: state.diagnosis.medications,
    patient: state.patients.patient,
    symptoms: state.symptoms.list,
    diagnosis: state.diagnosis.diagnosis
})

const mapDispatchToProps = dispatch => bindActionCreators({ makeDiagnosis, fetchPatients, fetchMedications, fetchPatient, fetchSymptoms, deleteDiagnosis }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(diagnosis);