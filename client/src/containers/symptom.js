import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchSymptom } from '../actions/symptoms';


class Symptom extends Component {

  componentDidMount(){
    this.props.fetchSymptom(this.props.id);
  }

  render() {
    const symptom = this.props;
    return (
      <div>
        {symptom.name}
      </div>
    )
  }
}

const mapStateToProps = state => ({
    symptom: state.symptoms.symptom
});

const mapDispatchToProps = dispatch => bindActionCreators({fetchSymptom}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Symptom);