import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchSymptoms, fetchSymptom, deleteSymptom, createSymptom } from '../actions/symptoms';
import { Table, TableHead, TableBody, Button, Input, MDBContainer } from 'mdbreact';

class Symptoms extends React.Component{
    
    componentDidMount(){
        this.props.fetchSymptoms();
    }

    render(){
        const { symptoms } = this.props;
        return(
                <Table small>
                    <TableHead color="blue-gradient" textWhite>
                        <tr>
                        <th>ID</th>
                        <th>Symptom</th>
                        <th>Edit/Delete</th>
                        </tr>
                    </TableHead>
                    <TableBody>
                        {
                            symptoms.map((symptom, id) => {
                                return(<tr key={id}>
                                    <td className="white-text">{symptom.id}</td>
                                    <td><Input className="white-text" value={symptom.name} /></td>
                                    <td><Button onClick={() => this.props.fetchSymptom(symptom.id)}> Save </Button>
                                    <Button color="danger" onClick={() => this.props.deleteSymptom(symptom.id)}> Delete </Button></td>
                                    </tr>
                                )
                            })
                        }
                    </TableBody>
                </Table>
        )
    }
}

const mapStateToProps = state => ({
    symptoms: state.symptoms.list,
    symptom: state.symptoms.symptom
});

const mapDispatchToProps = dispatch => bindActionCreators({ fetchSymptoms, fetchSymptom, deleteSymptom, createSymptom }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Symptoms);