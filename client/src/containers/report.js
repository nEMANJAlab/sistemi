import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getReport, getAddiction } from '../actions/diagnosis'
import { Button } from 'mdbreact';

class report extends Component {

    getReport = () => {
        this.props.getReport();
        this.props.getAddiction();
    }

    getAddiction = () => {
        
    }

    render() {
        const { report } = this.props;
        const { addiction } = this.props;
        return (
        <div>
            <br/>
            <p className="illnessPatient"> Hronicno oboljenje </p>
            {
                report.map((report, id) => {
                    return(
                    <div className="subText" key={id}>
                        <p className="illnessPatient">Pacijent: {report.name} {report.surname}, moguce hronicno oboljenje za bolest: </p>
                        {
                            report.diagnosisDTO[0].illnessesDTO.map((illness, id) => {
                                return( <div className="possibleIllness" key={id}> {illness.name} </div>)
                            })
                        }<br/>
                    </div>
                    )
                })
            }
            <p className="illnessPatient"> Zavisnost </p>
            {
                addiction.map((addiction, id) => {
                    return(
                    <div className="subText" key={id}>
                    
                        <p className="possibleIllness">Pacijent {addiction.name} {addiction.surname} ima mogucu zavisnost na analgetike. </p>
                        
                    </div>
                    )
                })
            }
            <Button color="success" onClick={this.getReport} >Izvestaj</Button>
        </div>
        )
    }
}

const mapStateToProps = state => ({
    report: state.diagnosis.report,
    addiction: state.diagnosis.addiction
})

const mapDispatchToProps = dispatch => bindActionCreators({ getReport, getAddiction }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(report);