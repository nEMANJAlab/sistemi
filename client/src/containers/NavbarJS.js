import React from 'react';
import { Navbar, NavbarBrand, NavbarNav, NavbarToggler, Collapse, NavItem, NavLink, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'mdbreact';

class NavbarJS extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collapse: false,
            isWideEnough: false,
        };
    this.onClick = this.onClick.bind(this);
    }

    onClick(){
        this.setState({
            collapse: !this.state.collapse,
        });
    }

    navActive(){

    }

    render() {
        return (
                <div>
                <Navbar color="blue" dark expand="md" scrolling style={{ opacity: 0.9 }}>
                    <NavbarBrand href="/">
                        <strong>SKPO</strong>
                    </NavbarBrand>
                    { !this.state.isWideEnough && <NavbarToggler onClick = { this.onClick } />}
                    <Collapse isOpen = { this.state.collapse } navbar>
                        <NavbarNav left>
                          <NavItem>
                              <NavLink to="/">Pocetna</NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink activeStyle={{ color: 'blue' }} to="/pacijenti">Pacijenti</NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink activeStyle={{ color: 'blue' }} to="/simptomi">Simptomi</NavLink>
                          </NavItem>
                          <NavItem>
                              <NavLink activeStyle={{ color: 'blue' }} to="/bolesti">Bolesti</NavLink>
                          </NavItem>
                          <NavItem>
                              <NavLink activeStyle={{ color: 'blue' }} to="/novkarton">Nov Karton</NavLink>
                          </NavItem>
                          <NavItem>
                             <NavLink activeStyle={{ color: 'blue' }} to="/izvestaj">Izvestaj</NavLink>
                          </NavItem>
                        </NavbarNav>
                        <NavbarNav right>
                          <NavItem>
                            <form className="form-inline md-form mt-0">
                              <input className="form-control mr-sm-2 mb-0" type="text" placeholder="Search" aria-label="Search"/>
                            </form>
                          </NavItem>
                        </NavbarNav>
                    </Collapse>
                    
                </Navbar>
                
                </div>
        );
    }
    
}

export default NavbarJS;