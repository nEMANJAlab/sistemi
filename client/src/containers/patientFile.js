import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { createPatient } from '../actions/patients'
import Select from 'react-select';

class PatientFile extends Component {
  constructor(props){
    super(props);
    this.state = {
      options: [{ value: "sastavA", label: "sastavA" },
      { value: "sastavA", label: "sastavA" },
      { value: "sastavA1", label: "sastavA1" },
      { value: "sastavA2", label: "sastavA2" },
      { value: "sastavA3", label: "sastavA3" },
      { value: "sastavB", label: "sastavB" },
      { value: "sastavB1", label: "sastavB1" },
      { value: "sastavB2", label: "sastavB2" },
      { value: "sastavB3", label: "sastavB3" },
      { value: "sastavC", label: "sastavC" },
      { value: "sastavC1", label: "sastavC1" },
      { value: "sastavC2", label: "sastavC2" },
      { value: "sastavC3", label: "sastavC3" },
      { value: "zelatin", label: "zelatin" },
      { value: "ulje ricinusa", label: "ulje ricinusa" },
      { value: "talk", label: "talk" },
      { value: "celuloza", label: "celuloza"} ],
      selectedAllergies: null,
    }
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
    console.log(this.state)
  }

  createFile = () => {
    this.props.createPatient(this.state)
  }

  addAllergy = (e) => {
    var allergies = []
    e.forEach(element => {
      allergies.push(element.value)
    });
    this.setState({ allergiesDTO: allergies})
  }

  render() {
    const { selectedOption } = this.state;
    return (
      <div className="container" style={{position: "absolute", right: 0, left: 0}}>
        <form>
            <p className="h4 text-center mb-4 white-text">Karton</p>
            <label htmlFor="defaultFormContactNameEx" className="white-text">Ime pacijenta</label>
            <input required id="name" onChange={this.handleChange} type="text" className="form-control"/>
            <br/>
            <label htmlFor="defaultFormContactEmailEx" className="white-text">Prezime pacijenta</label>
            <input required id="surname" onChange={this.handleChange} className="form-control"/>
            <br/>
            <label htmlFor="defaultFormContactSubjectEx" className="white-text">Godine pacijenta</label>
            <input required type="number" onChange={this.handleChange} id="age" className="form-control"/>
            <br/>
            <label htmlFor="defaultFormContactMessageEx" className="white-text">Alergije</label>
            <Select className="container" isMulti
                    value={selectedOption}
                    onChange={this.addAllergy}
                    options={this.state.options}
                />
            <div className="text-center mt-4">
                <button href="/jasta" className="btn btn-outline-warning" onClick={() => this.props.createPatient(this.state)} >Snimi<i className="fa fa-paper-plane-o ml-2"></i></button>
            </div>
            </form>
      </div>
    )
  }
}

const mapStateToProps = state => ({
    
})

const mapDispatchToProps = dispatch => bindActionCreators({ createPatient }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(PatientFile);