import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchPatients, fetchPatient } from '../actions/patients';
import { fetchSymptoms } from '../actions/symptoms';
import Patient from '../containers/patient';

class Patients extends Component {

  componentDidMount(){
    this.props.fetchPatients();
    this.props.fetchSymptoms();
  }

  getPatient = id => {
   this.props.fetchPatient(id);
  }

  render() {
    const { patients } = this.props;
    const { symptoms } = this.props;
    return (
      <div className="container">
        <div className="row">
          {
            patients.map((patient, id) => {
                return(
                    <Patient symptoms={symptoms} action={() => this.getPatient(patient.id)} key={id} patient={patient}/>
                )
            })
        }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
    patients: state.patients.list,
    symptoms: state.symptoms.list
})

const mapDispatchToProps = dispatch => bindActionCreators({ fetchPatients, fetchPatient, fetchSymptoms}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Patients);
