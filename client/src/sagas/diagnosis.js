import { MAKE_DIAGNOSIS, MAKE_DIAGNOSIS_SUCCESS, MAKE_DIAGNOSIS_FAILURE, FETCH_MEDICATIONS, FETCH_MEDICATIONS_FAILURE, FETCH_MEDICATIONS_SUCCESS, DELETE_DIAGNOSIS_SUCCESS, DELETE_DIAGNOSIS_FAILURE, DELETE_DIAGNOSIS, GET_REPORT_SUCCESS, GET_REPORT_FAILURE, GET_REPORT, GET_ADDICTION, GET_ADDICTION_SUCCESS, GET_ADDICTION_FAILURE } from '../actions/diagnosis';
import { call, put, takeLatest } from 'redux-saga/effects';
import { makeDiagnosis as MD } from '../api/diagnosis';     
import { fetchMedications as FM } from '../api/diagnosis';   
import { deleteDiagnosis as DD } from '../api/diagnosis';  
import { getReport as GR } from '../api/diagnosis';   
import { getAddiction as GA } from '../api/diagnosis';   

function* makeDiagnosis(action){
    try{
        const data = yield call(MD, action.payload);
        yield put({ type: MAKE_DIAGNOSIS_SUCCESS, payload: data})
    }catch(e){
        yield put({ type: MAKE_DIAGNOSIS_FAILURE });
    }
}

function* getReport(action){
    try{
        const data = yield call(GR);
        yield put({ type: GET_REPORT_SUCCESS, payload: data})
    }catch(e){
        yield put({ type: GET_REPORT_FAILURE });
    }
}

function* getAddiction(action){
    try{
        const data = yield call(GA);
        yield put({ type: GET_ADDICTION_SUCCESS, payload: data})
    }catch(e){
        yield put({ type: GET_ADDICTION_FAILURE });
    }
}

function* deleteDiagnosis(action){
    try{
        const data = yield call(DD, action.payload.id);
        yield put({ type: DELETE_DIAGNOSIS_SUCCESS, payload: data})
    }catch(e){
        yield put({ type: DELETE_DIAGNOSIS_FAILURE });
    }
}

function* fetchMedications(action){
    try{
        const data = yield call(FM);
        yield put({ type: FETCH_MEDICATIONS_SUCCESS, payload: data})
    }catch(e){
        yield put({ type: FETCH_MEDICATIONS_FAILURE})
    }
}

export default function* diagnosisSaga(){
    yield takeLatest(MAKE_DIAGNOSIS, makeDiagnosis);
    yield takeLatest(FETCH_MEDICATIONS, fetchMedications)
    yield takeLatest(DELETE_DIAGNOSIS, deleteDiagnosis)
    yield takeLatest(GET_REPORT, getReport)
    yield takeLatest(GET_ADDICTION, getAddiction)
}