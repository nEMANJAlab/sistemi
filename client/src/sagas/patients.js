import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';

import { fetchPatient as FP} from '../api/patients';
import { fetchPatients as FPs } from '../api/patients';
import { createPatient as CP } from '../api/patients';
import { deletePatient as DP } from '../api/patients';

import { FETCH_PATIENT, FETCH_PATIENT_SUCCESS, FETCH_PATIENT_FAILURE,
        FETCH_PATIENTS, FETCH_PATIENTS_SUCCESS, FETCH_PATIENTS_FAILURE,
        CREATE_PATIENT, CREATE_PATIENT_FAILURE, CREATE_PATIENT_SUCCESS,
        DELETE_PATIENT, DELETE_PATIENT_FAILURE, DELETE_PATIENT_SUCCESS } from '../actions/patients';

function* fetchPatient(action){
    try{
        const data = yield call(FP, action.payload.id);
        yield put({type: FETCH_PATIENT_SUCCESS, payload: data});
    }catch(e){
        yield put({type: FETCH_PATIENT_FAILURE});
    }
}

function* fetchPatients(action){
    try{
        const data = yield call(FPs);
        yield put({ type: FETCH_PATIENTS_SUCCESS, payload: data})
    }catch(e){
        yield put({ type: FETCH_PATIENTS_FAILURE })
    }
}

function* createPatient(action){
    try{
        const data = yield call(CP, action.payload.body);
        console.log(action.payload.body)
        console.log("SAGA")
        yield put({ type: CREATE_PATIENT_SUCCESS, payload: data});
    }catch(e){
        yield put({ type: CREATE_PATIENT_FAILURE});
    }
}

function* deletePatient(action){
    try{
        const data = call(DP, action.payload.id);
        yield put({ type: DELETE_PATIENT_SUCCESS, payload: data});
    }catch(e){
        yield put({type: DELETE_PATIENT_FAILURE});
    }
}

export default function* patientsSaga(){
    yield takeEvery(FETCH_PATIENTS, fetchPatients);
    yield takeLatest(FETCH_PATIENT, fetchPatient);
    yield takeLatest(CREATE_PATIENT, createPatient);
    yield takeLatest(DELETE_PATIENT, deletePatient);
}