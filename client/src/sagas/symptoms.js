import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';

import { fetchSymptoms as FSs } from '../api/symptoms';
import { fetchSymptom as FS } from '../api/symptoms';
import { deleteSymptom as DS } from '../api/symptoms';
import { createSymptom as CS } from '../api/symptoms';

import { FETCH_SYMPTOMS, FETCH_SYMPTOMS_SUCCESS, 
    FETCH_SYMPTOMS_FAILURE, CREATE_SYMPTOM, 
    CREATE_SYMPTOM_SUCCESS, CREATE_SYMPTOM_FAILURE, 
    DELETE_SYMPTOM, DELETE_SYMPTOM_SUCCESS, DELETE_SYMPTOM_FAILURE,
    FETCH_SYMPTOM, FETCH_SYMPTOM_FAILURE, FETCH_SYMPTOM_SUCCESS} 
    from '../actions/symptoms';

function* fetchSymptoms(action){
    try{
        const data = yield call(FSs);
        yield put({type: FETCH_SYMPTOMS_SUCCESS, payload: data});
    }catch(e){
        yield put({type: FETCH_SYMPTOMS_FAILURE});
    }
}

function* fetchSymptom(action){
    try{
        const data = yield call(FS, action.payload.id);
        yield put({type: FETCH_SYMPTOM_SUCCESS, payload: data})
    }catch(e){
        yield put({type: FETCH_SYMPTOM_FAILURE});
    }
}

function* createSymptom(action){
    try{
        yield call(CS);
        yield put({type: CREATE_SYMPTOM_SUCCESS})
    }
    catch(e){
        yield put({type: CREATE_SYMPTOM_FAILURE})
    }
}

function* deleteSymptom(action){
    try{
        const data = yield call(DS, action.payload.id);
        yield put({type: DELETE_SYMPTOM_SUCCESS, payload: data})
    }
    catch(e){
        yield put({type: DELETE_SYMPTOM_FAILURE})
    }
}

export default function* symptomsSaga(){
    yield takeEvery(FETCH_SYMPTOMS, fetchSymptoms);
    yield takeLatest(FETCH_SYMPTOM, fetchSymptom);
    yield takeLatest(DELETE_SYMPTOM, deleteSymptom);
    yield takeLatest(CREATE_SYMPTOM, createSymptom);
}