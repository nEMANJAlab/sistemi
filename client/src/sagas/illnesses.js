import { FETCH_ILLNESSES, FETCH_ILLNESSES_SUCCESS, 
    FETCH_ILLNESSES_FAILURE, CREATE_ILLNESS, 
    CREATE_ILLNESS_SUCCESS, CREATE_ILLNESS_FAILURE, 
    DELETE_ILLNESS, DELETE_ILLNESS_SUCCESS, DELETE_ILLNESS_FAILURE,
    FETCH_ILLNESS, FETCH_ILLNESS_FAILURE, FETCH_ILLNESS_SUCCESS, GET_SYMPTOMS_BY_ILLNESS_SUCCESS, GET_SYMPTOMS_BY_ILLNESS_FAILURE, GET_SYMPTOMS_BY_ILLNESS} 
    from '../actions/illnesses';

import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';

import { fetchIllnesses as FIs } from '../api/illnesses';
import { fetchIllness as FI } from '../api/illnesses';
import { createIllness as CI } from '../api/illnesses';
import { deleteIllness as DI } from '../api/illnesses';
import { fetchSymptomsByIllness as FSB } from '../api/illnesses';

function* fetchIllnesses(action){
    try{
        const data = yield call(FIs);
        yield put({type: FETCH_ILLNESSES_SUCCESS, payload: data});
    }catch(e){
        yield put({type: FETCH_ILLNESSES_FAILURE});
    }
}

function* getSymptoms(action){
    try{
        const data = yield call(FSB, action.payload.name);
        yield put({type: GET_SYMPTOMS_BY_ILLNESS_SUCCESS, payload: data});
    }catch(e){
        yield put({type: GET_SYMPTOMS_BY_ILLNESS_FAILURE});
    }
}

function* fetchIllness(action){
    try{
        const data = yield call(FI, action.payload.id);
        yield put({ type: FETCH_ILLNESS_SUCCESS, payload: data})
    }catch(e){
        yield put({type: FETCH_ILLNESS_FAILURE})
    }
}

function* createIllness(action){
    try{
        const data = yield call(CI, action.payload);
        yield put({ type: CREATE_ILLNESS_SUCCESS, payload: data})
    }catch(e){
        yield put({type: CREATE_ILLNESS_FAILURE})
    }
}

function* deleteIllness(action){
    try{
        const data = yield call(DI, action.payload.id);
        yield call({ type: DELETE_ILLNESS_SUCCESS, payload: data})
    }catch(e){
        yield put({type: DELETE_ILLNESS_FAILURE})
    }
}

export default function* symptomsSaga(){
    yield takeEvery(FETCH_ILLNESSES, fetchIllnesses);
    yield takeLatest(FETCH_ILLNESS, fetchIllness);
    yield takeLatest(CREATE_ILLNESS, createIllness);
    yield takeLatest(DELETE_ILLNESS, deleteIllness);
    yield takeLatest(GET_SYMPTOMS_BY_ILLNESS, getSymptoms);
}
