import illnesses from './illnesses';
import symptoms from './symptoms';
import patients from './patients';
import diagnosis from './diagnosis';
import { fork } from 'redux-saga/effects'

export default function* rootSaga () {
    yield [
        fork(illnesses), 
        fork(symptoms),
        fork(patients),
        fork(diagnosis)
    ];
}