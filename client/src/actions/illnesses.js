export const FETCH_ILLNESSES = 'illnesses/FETCH_ILLNESSES';
export const FETCH_ILLNESSES_SUCCESS = 'illnesses/FETCH_ILLNESSES_SUCCESS';
export const FETCH_ILLNESSES_FAILURE = 'illnesses/FETC_ILLNESSES_FAILURE';
export const FETCH_ILLNESS = 'illnesses/FETCH_ILLNESS';
export const FETCH_ILLNESS_SUCCESS = 'illnesses/FETCH_ILLNESS_SUCCESS';
export const FETCH_ILLNESS_FAILURE = 'illnesses/FETCH_ILLNESS_FAILURE';
export const CREATE_ILLNESS = 'illnesses/CREATE_ILLNESS';
export const CREATE_ILLNESS_SUCCESS = 'illnesses/CREATE_ILLNESS_SUCCESS';
export const CREATE_ILLNESS_FAILURE = 'illnesses/CREATE_ILLNESS_FAILURE';
export const DELETE_ILLNESS = 'illnesses/DELETE_ILLNESS';
export const DELETE_ILLNESS_SUCCESS = 'illnesses/DELETE_ILLNESS_SUCCESS';
export const DELETE_ILLNESS_FAILURE = 'illnesses/DELETE_ILLNESS_FAILURE';
export const GET_SYMPTOMS_BY_ILLNESS = 'illnesses/GET_SYMPTOMS_BY_ILLNESS';
export const GET_SYMPTOMS_BY_ILLNESS_SUCCESS = 'illnesses/GET_SYMPTOMS_BY_ILLNESS_SUCCESS';
export const GET_SYMPTOMS_BY_ILLNESS_FAILURE = 'illnesses/GET_SYMPTOMS_BY_ILLNESS_FAILURE';

export const fetchIllnesses = () => ({
    type: FETCH_ILLNESSES 
});

export const getSymptoms = (name) => ({
    type: GET_SYMPTOMS_BY_ILLNESS,
    payload: {name}
});

export const fetchIllness = id => ({
    type: FETCH_ILLNESS,
    payload: {id}
});