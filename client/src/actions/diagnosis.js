export const MAKE_DIAGNOSIS = "MAKE_DIAGNOSIS";
export const MAKE_DIAGNOSIS_SUCCESS = "MAKE_DIAGNOSIS_SUCCESS";
export const MAKE_DIAGNOSIS_FAILURE = "MAKE_DIAGNOSIS_FAILURE";

export const DELETE_DIAGNOSIS = "DELETE_DIAGNOSIS";
export const DELETE_DIAGNOSIS_SUCCESS = "DELETE_DIAGNOSIS_SUCCESS";
export const DELETE_DIAGNOSIS_FAILURE = "DELETE_DIAGNOSIS_FAILURE";

export const FETCH_MEDICATIONS = "FETCH_MEDICATIONS";
export const FETCH_MEDICATIONS_SUCCESS = "FETCH_MEDICATIONS_SUCCESS";
export const FETCH_MEDICATIONS_FAILURE = "FETCH_MEDICATIONS_FAILURE";

export const GET_REPORT = "GET_REPORT";
export const GET_REPORT_SUCCESS = "GET_REPORT_SUCCESS";
export const GET_REPORT_FAILURE = "GET_REPORT_FAILURE";

export const GET_ADDICTION = "GET_ADDICTION";
export const GET_ADDICTION_SUCCESS = "GET_ADDICTION_SUCCESS";
export const GET_ADDICTION_FAILURE = "GET_ADDICTION_FAILURE";

export const makeDiagnosis = (diagnosis) => ({
    type: MAKE_DIAGNOSIS,
    payload: {diagnosis}
})

export const getReport = () => ({
    type: GET_REPORT
})

export const getAddiction = () => ({
    type: GET_ADDICTION
})

export const deleteDiagnosis = id => ({
    type: DELETE_DIAGNOSIS,
    payload: {id}
})

export const fetchMedications = () => ({
    type: FETCH_MEDICATIONS
})