export const FETCH_SYMPTOMS = 'symptoms/FETCH_SYMPTOMS';
export const FETCH_SYMPTOMS_SUCCESS = 'symptoms/FETCH_SYMPTOMS_SUCCESS';
export const FETCH_SYMPTOMS_FAILURE = 'symptoms/FETC_SYMPTOMS_FAILURE';
export const FETCH_SYMPTOM = 'symptoms/FETCH_SYMPTOM';
export const FETCH_SYMPTOM_SUCCESS = 'symptoms/FETCH_SYMPTOM_SUCCESS';
export const FETCH_SYMPTOM_FAILURE = 'symptoms/FETCH_SYMPTOM_FAILURE';
export const CREATE_SYMPTOM = 'symptoms/CREATE_SYMPTOM';
export const CREATE_SYMPTOM_SUCCESS = 'symptoms/CREATE_SYMPTOM_SUCCESS';
export const CREATE_SYMPTOM_FAILURE = 'symptoms/CREATE_SYMPTOM_FAILURE';
export const DELETE_SYMPTOM = 'symptoms/DELETE_SYMPTOM';
export const DELETE_SYMPTOM_SUCCESS = 'symptoms/DELETE_SYMPTOM_SUCCESS';
export const DELETE_SYMPTOM_FAILURE = 'symptoms/DELETE_SYMPTOM_FAILURE';

export const fetchSymptoms = () => ({
    type: FETCH_SYMPTOMS 
});

export const fetchSymptom = id => ({
    type: FETCH_SYMPTOM,
    payload: {id}
});

export const createSymptom = body => ({
    type: CREATE_SYMPTOM,
    payload: {body}
});

export const deleteSymptom = id => ({
    type: DELETE_SYMPTOM,
    payload: {id}
});