export const FETCH_PATIENTS = 'patients/FETCH_PATIENTS';
export const FETCH_PATIENTS_SUCCESS = 'patients/FETCH_PATIENTS_SUCCESS';
export const FETCH_PATIENTS_FAILURE = 'patients/FETCH_PATIENTS_FAILURE';
export const FETCH_PATIENT = 'patient/FETCH_PATIENT';
export const FETCH_PATIENT_SUCCESS = 'patient/FETCH_PATIENT_SUCCESS';
export const FETCH_PATIENT_FAILURE = 'patient/FETCH_PATIENT_FAILURE';
export const CREATE_PATIENT = 'patient/CREATE_PATIENT';
export const CREATE_PATIENT_SUCCESS = 'patient/CREATE_PATIENT_SUCCESS';
export const CREATE_PATIENT_FAILURE = 'patient/CREATE_PATIENT_FAILURE';
export const DELETE_PATIENT = 'patients/DELETE_PATIENT';
export const DELETE_PATIENT_SUCCESS = 'patients/DELETE_PATIENT_SUCCESS';
export const DELETE_PATIENT_FAILURE = 'patients/DELETE_PATIENT_FAILURE';

export const fetchPatient = id => ({
    type: FETCH_PATIENT,
    payload: {id}
});

export const createPatient = body => ({
    type: CREATE_PATIENT,
    payload: {body}
})

export const deletePatient = id => ({
    type: DELETE_PATIENT,
    payload: {id}
})

export const fetchPatients = () => ({
    type: FETCH_PATIENTS
});