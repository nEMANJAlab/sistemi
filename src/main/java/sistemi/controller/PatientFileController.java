package sistemi.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sistemi.dto.PatientFileDTO;
import sistemi.model.Illness;
import sistemi.model.PatientFile;
import sistemi.model.Report;
import sistemi.service.PatientFileService;

@CrossOrigin(origins = "*")
@Controller
public class PatientFileController {

	@Autowired 
	PatientFileService patientService;
	
	@RequestMapping(value= "/patients", method=RequestMethod.GET)
	public ResponseEntity<List<PatientFileDTO>> getPatients(){
		return new ResponseEntity<>(patientService.getAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value= "/patient/{id}", method=RequestMethod.GET)
	public ResponseEntity<PatientFileDTO> getPatient(@PathVariable Long id){
		return new ResponseEntity<>(patientService.getPatient(id), HttpStatus.OK);
	}
	
	@RequestMapping(value= "/patient", method=RequestMethod.POST)
	public ResponseEntity<PatientFileDTO> createPatientFile(@RequestBody PatientFileDTO patientDTO){
		return new ResponseEntity<>(patientService.create(patientDTO), HttpStatus.OK);
	}
	
	@RequestMapping(value= "/patient/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<List<PatientFileDTO>> deletePatient(@PathVariable Long id){
		return new ResponseEntity<>(patientService.delete(id),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/hronicno", method = RequestMethod.GET)
	public ResponseEntity<List<PatientFileDTO>> getHronic(){
		return new ResponseEntity<>(patientService.getHronic(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/zavisnost", method = RequestMethod.GET)
	public ResponseEntity<List<PatientFileDTO>> getAddiction(){
		return new ResponseEntity<>(patientService.getAddiction(), HttpStatus.OK);
	}
	
}
