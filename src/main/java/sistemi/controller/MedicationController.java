package sistemi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sistemi.dto.MedicationDTO;
import sistemi.service.MedicationService;

@CrossOrigin(origins = "*")
@Controller
public class MedicationController {

	@Autowired
	private MedicationService medicationService;
	
	@RequestMapping(value = "/medications", method = RequestMethod.GET)
	public ResponseEntity<List<MedicationDTO>> getAll() {
		
		return new ResponseEntity<>(medicationService.getAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/medication/{name}", method = RequestMethod.GET)
	public ResponseEntity<MedicationDTO> getMedication(@PathVariable String name) {

		MedicationDTO medicationDTO = medicationService.getMedication(name);
		if (medicationDTO == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		return new ResponseEntity<>(medicationDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/medication", method=RequestMethod.POST)
	public ResponseEntity<HttpStatus> create(@RequestBody MedicationDTO medicationDTO){
		if (medicationService.create(medicationDTO))
			return new ResponseEntity<>(HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
}
