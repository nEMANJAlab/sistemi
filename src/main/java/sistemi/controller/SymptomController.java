package sistemi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sistemi.dto.SymptomDTO;
import sistemi.service.SymptomService;

@CrossOrigin(origins = "*")
@Controller
public class SymptomController {

	@Autowired
	SymptomService symptomService;
	
	@RequestMapping(value= "/symptoms", method=RequestMethod.GET)
	public ResponseEntity<List<SymptomDTO>> getSymptom(){
		return new ResponseEntity<>(symptomService.getAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value= "/symptom/{id}", method=RequestMethod.GET)
	public ResponseEntity<SymptomDTO> getSymptom(@PathVariable Long id){
		
		SymptomDTO symptomDTO = symptomService.getSymptom(id);
		
		return new ResponseEntity<>(symptomDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value= "/symptom", method=RequestMethod.POST)
	public ResponseEntity<HttpStatus> create(@RequestBody SymptomDTO symptomDTO){
		
		symptomService.create(symptomDTO);
		
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}
	
	@RequestMapping(value= "/symptom/{id}", method=RequestMethod.PUT)
	public ResponseEntity<HttpStatus> update(@RequestBody SymptomDTO symptomDTO, @PathVariable Long id){
		symptomService.update(symptomDTO, id);
	
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/symptom/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<List<SymptomDTO>> delete(@PathVariable Long id){
		symptomService.delete(id);
		return new ResponseEntity<>(symptomService.getAll(),HttpStatus.OK);
	}

}
