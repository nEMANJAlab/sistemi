package sistemi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sistemi.dto.IllnessDTO;
import sistemi.dto.SymptomDTO;
import sistemi.service.IllnessService;
import sistemi.service.SymptomService;

//@CrossOrigin(origins = {"http://localhost:3000", "https://d0ff974c.ngrok.io"})
@CrossOrigin(origins = "*")
@Controller
public class IllnessController {

	@Autowired
	private IllnessService illnessService;

	@Autowired
	private SymptomService symptomService;
	
	@RequestMapping(value = "/illnesses", method = RequestMethod.GET)
	public ResponseEntity<List<IllnessDTO>> getAll() {
		
		return new ResponseEntity<>(illnessService.getAll(), HttpStatus.OK);
	}

	
	@RequestMapping(value = "/illness/{name}", method = RequestMethod.GET)
	public ResponseEntity<IllnessDTO> getIllness(@PathVariable String name) {

		IllnessDTO illnessDTO = illnessService.getIllness(name);
		if (illnessDTO == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		return new ResponseEntity<>(illnessDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/illness/{name}/symptoms", method = RequestMethod.GET)
	public ResponseEntity<List<SymptomDTO>> getSymptoms(@PathVariable String name) {
		return new ResponseEntity<List<SymptomDTO>>(symptomService.getSymptomsByIllness(name), HttpStatus.OK);

	}
	
	@RequestMapping(value="/illness", method=RequestMethod.POST)
	public ResponseEntity<HttpStatus> create(@RequestBody IllnessDTO illnessDTO){
		if (illnessService.create(illnessDTO))
			return new ResponseEntity<>(HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	@RequestMapping(value = "/illness/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<List<IllnessDTO>> delete(@PathVariable Long id) {
		return new ResponseEntity<>(illnessService.delete(id), HttpStatus.OK);

	}

}
