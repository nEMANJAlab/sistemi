package sistemi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sistemi.dto.TherapyDTO;
import sistemi.service.TherapyService;

@CrossOrigin(origins = "*")
@Controller
public class TherapyController {

	@Autowired
	private TherapyService therapyService;
	
	
	@RequestMapping(value = "/therapy", method = RequestMethod.GET)
	public ResponseEntity<List<TherapyDTO>> getAll() {
		
		return new ResponseEntity<>(therapyService.getAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/therapy/{id}", method = RequestMethod.GET)
	public ResponseEntity<TherapyDTO> getIllness(@PathVariable Long id) {

		TherapyDTO therapyDTO = therapyService.getTherapy(id);
		if (therapyDTO == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		return new ResponseEntity<>(therapyDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/therapy", method=RequestMethod.POST)
	public ResponseEntity<HttpStatus> create(@RequestBody TherapyDTO therapyDTO){
		if (therapyService.create(therapyDTO))
			return new ResponseEntity<>(HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	
}
