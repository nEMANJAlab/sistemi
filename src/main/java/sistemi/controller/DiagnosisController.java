package sistemi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sistemi.dto.DiagnosisDTO;
import sistemi.dto.PatientFileDTO;
import sistemi.service.DiagnosisService;

@Controller
@CrossOrigin(origins = "*")
public class DiagnosisController {
	
	@Autowired
	DiagnosisService diagnosisService;
	
	@RequestMapping(value= "/diagnosis", method=RequestMethod.POST)
	public ResponseEntity<DiagnosisDTO> makeDiagnosis(@RequestBody DiagnosisDTO diagnosisDTO){
		return new ResponseEntity<>(diagnosisService.create(diagnosisDTO), HttpStatus.OK);
	}
	
	@RequestMapping(value= "/diagnosis", method=RequestMethod.GET)
	public ResponseEntity<List<DiagnosisDTO>> getAll(){
		return new ResponseEntity<>(diagnosisService.getAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/diagnosis/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<List<DiagnosisDTO>> delete(@PathVariable Long id) {
		return new ResponseEntity<>(diagnosisService.delete(id), HttpStatus.OK);

	}
	
	

}
