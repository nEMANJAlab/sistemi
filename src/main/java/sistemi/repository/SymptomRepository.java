package sistemi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sistemi.model.Symptom;

public interface SymptomRepository extends JpaRepository<Symptom, Long>{

	Symptom findByName(String name);
	Symptom findById(Long id);
}
