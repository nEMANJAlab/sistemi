package sistemi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sistemi.model.Medication;

public interface MedicationRepository extends JpaRepository<Medication, Long>{
	
	Medication findById(Long id);
	Medication findByName(String name);
	Medication findByType(String type);

}
