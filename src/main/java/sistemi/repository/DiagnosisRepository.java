package sistemi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sistemi.model.Diagnosis;

public interface DiagnosisRepository extends JpaRepository<Diagnosis, Long> {

	Diagnosis findById(Long id);
		
}
