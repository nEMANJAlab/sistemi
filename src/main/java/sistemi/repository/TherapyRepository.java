package sistemi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sistemi.model.Therapy;

public interface TherapyRepository extends JpaRepository<Therapy, Long>{
	
	Therapy findById(Long id);
	Therapy findByDiagnosisId(Long id);

}
