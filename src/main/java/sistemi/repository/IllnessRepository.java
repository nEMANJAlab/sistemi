package sistemi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sistemi.model.Illness;

@Repository
public interface IllnessRepository extends JpaRepository<Illness, Long> {
	
	Illness findByName(String name);
	Illness findById(Long id);
	

}
