package sistemi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sistemi.model.PatientFile;

public interface PatientFileRepository extends JpaRepository<PatientFile, Long>{

	PatientFile findByName(String name);
	PatientFile findById(Long id);
}
