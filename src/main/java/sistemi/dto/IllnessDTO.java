package sistemi.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import sistemi.model.Illness;

public class IllnessDTO {

	private Long id;
	private String name;
	private List<SymptomDTO> symptomsDTO = new ArrayList<SymptomDTO>();
	
	public IllnessDTO() {}
	
	public IllnessDTO(Illness i) {
		this.id = i.getId();
		this.name = i.getName();
		this.symptomsDTO = i.getSymptoms().stream().map(symptom -> new SymptomDTO(symptom)).collect(Collectors.toList());
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public List<SymptomDTO> getSymptoms() {
		return symptomsDTO;
	}

	public void setSymptoms(List<SymptomDTO> symptoms) {
		this.symptomsDTO = symptoms;
	}

	
	
}
