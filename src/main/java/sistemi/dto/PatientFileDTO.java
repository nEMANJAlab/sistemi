package sistemi.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import sistemi.model.Diagnosis;
import sistemi.model.PatientFile;

public class PatientFileDTO {

	private Long id;
	private String name;
	private String surname;
	private int age;
	private List<DiagnosisDTO> diagnosisDTO = new ArrayList<DiagnosisDTO>();
	private List<String> allergies = new ArrayList<String>();
	
	public PatientFileDTO() {}
	
	public PatientFileDTO(PatientFile pf) {
		this.id = pf.getId();
		this.name = pf.getName();
		this.surname = pf.getSurname();
		this.age = pf.getAge();
		this.diagnosisDTO = pf.getDiagnosis().stream()
				.map(diagnosis -> new DiagnosisDTO(diagnosis)).collect(Collectors.toList());
		this.allergies = pf.getAllergies();
		
	}

	public List<DiagnosisDTO> getDiagnosisDTO() {
		return diagnosisDTO;
	}

	public void setDiagnosisDTO(List<DiagnosisDTO> diagnosisDTO) {
		this.diagnosisDTO = diagnosisDTO;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public List<String> getAllergies() {
		return allergies;
	}

	public void setAllergies(List<String> allergies) {
		this.allergies = allergies;
	}
	
	
	
	
}
