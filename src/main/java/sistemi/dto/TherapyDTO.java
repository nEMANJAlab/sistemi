package sistemi.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import sistemi.model.Therapy;

public class TherapyDTO {
	
	private Long id;
	private List<MedicationDTO> medicationsDTO = new ArrayList<MedicationDTO>();
	
	public TherapyDTO() {};
	
	public TherapyDTO(Therapy t) {
		this.id = t.getId();
		this.medicationsDTO = t.getMedications().stream()
				.map(medication -> new MedicationDTO(medication)).collect(Collectors.toList());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<MedicationDTO> getMedicationsDTO() {
		return medicationsDTO;
	}

	public void setMedicationsDTO(List<MedicationDTO> medicationsDTO) {
		this.medicationsDTO = medicationsDTO;
	}

	
}
