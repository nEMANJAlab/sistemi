package sistemi.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import sistemi.model.Symptom;

public class SymptomDTO {

	private String name;
	private Long id;
	private List<IllnessDTO> illnessesDTO = new ArrayList<IllnessDTO>();
	
	public SymptomDTO() {}
	
	public SymptomDTO(Symptom s) {
		this.id = s.getId();
		this.name = s.getName();
		//this.illnessesDTO = s.getIllnesses().stream().map(illness -> new IllnessDTO(illness)).collect(Collectors.toList());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<IllnessDTO> getIllnesses() {
		return illnessesDTO;
	}

	public void setIllnesses(List<IllnessDTO> illnesses) {
		this.illnessesDTO = illnesses;
	}

	
	
}
