package sistemi.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import sistemi.model.Diagnosis;
import sistemi.repository.DiagnosisRepository;

public class DiagnosisDTO {
	
	private Long id;
	private Long patientId;
	private List<SymptomDTO> symptomsDTO = new ArrayList<SymptomDTO>();
	private List<IllnessDTO> illnessesDTO = new ArrayList<IllnessDTO>();
	private List<TherapyDTO> therapyDTO = new ArrayList<TherapyDTO>();
	private List<MedicationDTO> medicationsDTO = new ArrayList<MedicationDTO>();
	
	public DiagnosisDTO() {}
	
	public DiagnosisDTO(Diagnosis d) {
		this.id = d.getId();
		this.symptomsDTO = d.getSymptoms().stream()
				.map(symptom -> new SymptomDTO(symptom)).collect(Collectors.toList());
		this.illnessesDTO = d.getIllnesses().stream()
				.map(illness -> new IllnessDTO(illness)).collect(Collectors.toList());
		this.therapyDTO = d.getTherapies().stream()
				.map(therapy -> new TherapyDTO(therapy)).collect(Collectors.toList());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<SymptomDTO> getSymptomsDTO() {
		return symptomsDTO;
	}

	public void setSymptomsDTO(List<SymptomDTO> symptomsDTO) {
		this.symptomsDTO = symptomsDTO;
	}

	public List<IllnessDTO> getIllnessesDTO() {
		return illnessesDTO;
	}

	public void setIllnessesDTO(List<IllnessDTO> illnessesDTO) {
		this.illnessesDTO = illnessesDTO;
	}

	public List<TherapyDTO> getTherapyDTO() {
		return therapyDTO;
	}

	public void setTherapyDTO(List<TherapyDTO> therapyDTO) {
		this.therapyDTO = therapyDTO;
	}

	public List<MedicationDTO> getMedicationsDTO() {
		return medicationsDTO;
	}

	public void setMedicationsDTO(List<MedicationDTO> medicationsDTO) {
		this.medicationsDTO = medicationsDTO;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	
	

}
