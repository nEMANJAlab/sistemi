package sistemi.dto;

import java.util.ArrayList;
import java.util.List;

import sistemi.model.Medication;

public class MedicationDTO {
	
	private Long id;
	private String name;
	private String type;
	private List<String> ingredients = new ArrayList<String>();
	
	public MedicationDTO() {}
	
	public MedicationDTO(Medication m) {
		this.id = m.getId();
		this.name = m.getName();
		this.type = m.getType();
		this.ingredients = m.getIngredients();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<String> ingredients) {
		this.ingredients = ingredients;
	}

	
}
