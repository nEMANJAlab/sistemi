package sistemi;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import sistemi.model.Diagnosis;
import sistemi.model.Illness;
import sistemi.model.Medication;
import sistemi.model.PatientFile;
import sistemi.model.Symptom;
import sistemi.model.Therapy;
import sistemi.repository.DiagnosisRepository;
import sistemi.repository.IllnessRepository;
import sistemi.repository.MedicationRepository;
import sistemi.repository.PatientFileRepository;
import sistemi.repository.SymptomRepository;
import sistemi.repository.TherapyRepository;

@Component
public class InitData implements ApplicationRunner {
	
	@Autowired
	IllnessRepository illnessRepo;
	
	@Autowired
	SymptomRepository symptomRepo;
	
	@Autowired
	PatientFileRepository patientRepo;
	
	@Autowired
	DiagnosisRepository diagRepo;

	@Autowired
	MedicationRepository medicationRepo;
	
	@Autowired
	TherapyRepository therapyRepo;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {

		
		
		Symptom symptom = new Symptom();
		symptom.setName("Kasalj");
		symptomRepo.save(symptom);
		Symptom symptom1 = new Symptom();
		symptom1.setName("Curenje iz nosa");
		symptomRepo.save(symptom1);
		Symptom symptom2 = new Symptom();
		symptom2.setName("Bol u grlu");
		symptomRepo.save(symptom2);
		Symptom symptom3 = new Symptom();
		symptom3.setName("Kijanje");
		symptomRepo.save(symptom3);
		
		Symptom symptom5 = new Symptom();
		symptom5.setName("Glavobolja");
		symptomRepo.save(symptom5);
		Symptom symptom6 = new Symptom();
		symptom6.setName("Drhtavica");
		symptomRepo.save(symptom6);
		Symptom symptom7 = new Symptom();
		symptom7.setName("Bol koji se siri do usiju");
		symptomRepo.save(symptom7);
		Symptom symptom8 = new Symptom();
		symptom8.setName("Gubitak apetita");
		symptomRepo.save(symptom8);
		Symptom symptom9 = new Symptom();
		symptom9.setName("Umor");
		symptomRepo.save(symptom9);
		Symptom symptom10 = new Symptom();
		symptom10.setName("Zuti sekret iz nosa");
		symptomRepo.save(symptom10);
		Symptom symptom11 = new Symptom();
		symptom11.setName("Oticanje oko ociju");
		symptomRepo.save(symptom11);
		Symptom symptom12 = new Symptom();
		symptom12.setName("Cesto uriniranje");
		symptomRepo.save(symptom12);
		Symptom symptom13 = new Symptom();
		symptom13.setName("Gubitak telesne tezine");
		symptomRepo.save(symptom13);
		Symptom symptom14 = new Symptom();
		symptom14.setName("Zamor");
		symptomRepo.save(symptom14);
		Symptom symptom15 = new Symptom();
		symptom15.setName("Mucnina i povracanje");
		symptomRepo.save(symptom15);
		Symptom symptom16 = new Symptom();
		symptom16.setName("Nocturia");
		symptomRepo.save(symptom16);
		Symptom symptom17 = new Symptom();
		symptom17.setName("Otoci nogu i zglobova");
		symptomRepo.save(symptom17);
		Symptom symptom18 = new Symptom();
		symptom18.setName("Gusenje");
		symptomRepo.save(symptom18);
		Symptom symptom19 = new Symptom();
		symptom19.setName("Bol u grudima");
		symptomRepo.save(symptom19);
		Symptom symptom20 = new Symptom();
		symptom20.setName("Dijareja");
		symptomRepo.save(symptom20);
		Symptom symptom21 = new Symptom();
		symptom21.setName("Temperatura veca od 38");
		symptomRepo.save(symptom21);
		Symptom symptom22 = new Symptom();
		symptom22.setName("Temperatura od 40 do 41");
		symptomRepo.save(symptom22);
		Symptom symptom23 = new Symptom();
		symptom23.setName("Visok pritisak");
		symptomRepo.save(symptom23);
		
		Illness ila = new Illness();
		ila.setName("None");
		illnessRepo.save(ila);
		
		Illness il = new Illness();
		il.setName("Prehlada");
		il.getSymptoms().add(symptomRepo.findByName("Curenje iz nosa"));
		il.getSymptoms().add(symptomRepo.findByName("Bol u grlu"));
		il.getSymptoms().add(symptomRepo.findByName("Glavobolja"));
		il.getSymptoms().add(symptomRepo.findByName("Kasalj"));
		il.getSymptoms().add(symptomRepo.findByName("Kijanje"));
		illnessRepo.save(il);
		Illness il1 = new Illness();
		il1.setName("Groznica");
		il1.getSymptoms().add(symptomRepo.findByName("Curenje iz nosa"));
		il1.getSymptoms().add(symptomRepo.findByName("Bol u grlu"));
		il1.getSymptoms().add(symptomRepo.findByName("Glavobolja"));
		il1.getSymptoms().add(symptomRepo.findByName("Kasalj"));
		il1.getSymptoms().add(symptomRepo.findByName("Kijanje"));
		il1.getSymptoms().add(symptomRepo.findByName("Temperatura veca od 38"));
		il1.getSymptoms().add(symptomRepo.findByName("Drhtavica"));
		illnessRepo.save(il1);
		Illness il2 = new Illness();
		il2.setName("Upala krajnika");
		il2.getSymptoms().add(symptomRepo.findByName("Bol u grlu"));
		il2.getSymptoms().add(symptomRepo.findByName("Bol koji se siri do usiju"));
		il2.getSymptoms().add(symptomRepo.findByName("Glavobolja"));
		il2.getSymptoms().add(symptomRepo.findByName("Temperatura od 40 do 41"));
		il2.getSymptoms().add(symptomRepo.findByName("Drhtavica"));
		il2.getSymptoms().add(symptomRepo.findByName("Gubitak apetita"));
		il2.getSymptoms().add(symptomRepo.findByName("Umor"));
		il2.getSymptoms().add(symptomRepo.findByName("Zuti sekret iz nosa"));
		illnessRepo.save(il2);
		Illness il3 = new Illness();
		il3.setName("Sinusna infekcija");
		il3.getSymptoms().add(symptomRepo.findByName("Oticanje oko ociju"));
		il3.getSymptoms().add(symptomRepo.findByName("Glavobolja"));
		il3.getSymptoms().add(symptomRepo.findByName("Zuti sekret iz nosa"));
		il3.getSymptoms().add(symptomRepo.findByName("Bol u grlu"));
		il3.getSymptoms().add(symptomRepo.findByName("Temperatura veca od 38"));
		il3.getSymptoms().add(symptomRepo.findByName("Kasalj"));
		illnessRepo.save(il3);
		Illness il4 = new Illness();
		il4.setName("Hipertenzija");
		il4.getSymptoms().add(symptomRepo.findByName("Visok pritisak"));
		illnessRepo.save(il4);
		Illness il5 = new Illness();
		il5.setName("Dijabetes");
		il5.getSymptoms().add(symptomRepo.findByName("Cesto uriniranje"));
		il5.getSymptoms().add(symptomRepo.findByName("Gubitak telesne tezine"));
		il5.getSymptoms().add(symptomRepo.findByName("Zamor"));
		il5.getSymptoms().add(symptomRepo.findByName("Mucnina i povracanje"));
		illnessRepo.save(il5);
		Illness il6 = new Illness();
		il6.setName("Hronicna bubrezna bolest");
		il6.getSymptoms().add(symptomRepo.findByName("Zamor"));
		il6.getSymptoms().add(symptomRepo.findByName("Nocturia"));
		il6.getSymptoms().add(symptomRepo.findByName("Otoci nogu i zglobova"));
		il6.getSymptoms().add(symptomRepo.findByName("Gusenje"));
		il6.getSymptoms().add(symptomRepo.findByName("Bol u grudima"));
		illnessRepo.save(il6);
		Illness il7 = new Illness();
		il7.setName("Akutna bubrezna povreda");
		il7.getSymptoms().add(symptomRepo.findByName("Zamor"));
		il7.getSymptoms().add(symptomRepo.findByName("Gusenje"));
		il7.getSymptoms().add(symptomRepo.findByName("Otoci nogu i zglobova"));
		il7.getSymptoms().add(symptomRepo.findByName("Dijareja"));
		illnessRepo.save(il7);
		
		Medication medication = new Medication();
		medication.setName("Febricet");
		medication.setType("analgetik");
		List<String> ingredients = new ArrayList<String>();
		ingredients.add("zelatin");
		ingredients.add("talk");
		ingredients.add("ulje ricinusa");
		ingredients.add("celuloza");
		medication.setIngredients(ingredients);
		medicationRepo.save(medication);
		
		Medication medication1 = new Medication();
		List<String> ingredients1 = new ArrayList<String>();
		medication1.setName("Lek2");
		medication1.setType("analgetik");
		ingredients1.add("sastavA");
		ingredients1.add("sastavA1");
		ingredients1.add("sastavA2");
		ingredients1.add("sastavA3");
		medication1.setIngredients(ingredients1);
		medicationRepo.save(medication1);
		
		Medication medication2 = new Medication();
		List<String> ingredients2 = new ArrayList<String>();
		medication2.setName("Lek3");
		medication2.setType("antibiotik");
		ingredients2.add("sastavB");
		ingredients2.add("sastavB1");
		ingredients2.add("sastavB2");
		ingredients2.add("sastavB3");
		medication2.setIngredients(ingredients2);
		medicationRepo.save(medication2);
		
		Medication medication3 = new Medication();
		List<String> ingredients3 = new ArrayList<String>();
		medication3.setName("Lek4");
		medication3.setType("antibiotik");
		ingredients3.add("sastavC");
		ingredients3.add("sastavC1");
		ingredients3.add("sastavC2");
		ingredients3.add("sastavC3");
		medication3.setIngredients(ingredients3);
		medicationRepo.save(medication3);
		
		Therapy therapy = new Therapy();
		therapy.getMedications().add(medicationRepo.findByName("Febricet"));
		therapy.getMedications().add(medicationRepo.findByName("Lek2"));
		therapyRepo.save(therapy);
		
		Therapy therapy1 = new Therapy();
		therapy1.getMedications().add(medicationRepo.findByName("Febricet"));
		therapy1.getMedications().add(medicationRepo.findByName("Lek4"));
		therapy1.getMedications().add(medicationRepo.findByName("Lek3"));
		therapyRepo.save(therapy1);
		
		Therapy therapy2 = new Therapy();
		therapy2.getMedications().add(medicationRepo.findByName("Lek3"));
		therapy2.getMedications().add(medicationRepo.findByName("Lek2"));
		therapy2.getMedications().add(medicationRepo.findByName("Febricet"));
		therapy2.getMedications().add(medicationRepo.findByName("Lek4"));
		therapyRepo.save(therapy2);
		
		Therapy therapy3 = new Therapy();
		therapy3.getMedications().add(medicationRepo.findByName("Febricet"));
		therapyRepo.save(therapy3);
		
		Diagnosis diag = new Diagnosis();
		diag.getIllnesses().add(illnessRepo.findByName("Prehlada"));
		diag.getSymptoms().add(symptomRepo.findByName("Kijanje"));
		diag.getSymptoms().add(symptomRepo.findByName("Kasalj"));
		diag.getSymptoms().add(symptomRepo.findByName("Curenje iz nosa"));
		diag.getSymptoms().add(symptomRepo.findByName("Glavobolja"));
		diag.getSymptoms().add(symptomRepo.findByName("Bol u grlu"));
		diag.getTherapies().add(therapyRepo.findById((long) 1));
		diagRepo.save(diag);
		
		Diagnosis diag1 = new Diagnosis();
		diag1.getIllnesses().add(illnessRepo.findByName("Groznica"));
		diag1.getSymptoms().add(symptomRepo.findByName("Kijanje"));
		diag1.getSymptoms().add(symptomRepo.findByName("Kasalj"));
		diag1.getSymptoms().add(symptomRepo.findByName("Curenje iz nosa"));
		diag1.getSymptoms().add(symptomRepo.findByName("Glavobolja"));
		diag1.getSymptoms().add(symptomRepo.findByName("Temperatura veca od 38"));
		diag1.getSymptoms().add(symptomRepo.findByName("Drhtavica"));
		diag1.getSymptoms().add(symptomRepo.findByName("Bol u grlu"));
		diag1.getTherapies().add(therapyRepo.findById((long) 4));
		diagRepo.save(diag1);
		
		Diagnosis diag2 = new Diagnosis();
		diag2.getIllnesses().add(illnessRepo.findByName("Upala krajnika"));
		diag2.getSymptoms().add(symptomRepo.findByName("Glavobolja"));
		diag2.getSymptoms().add(symptomRepo.findByName("Bol koji se siri do usiju"));
		diag2.getSymptoms().add(symptomRepo.findByName("Bol u grlu"));
		diag2.getSymptoms().add(symptomRepo.findByName("Temperatura od 40 do 41"));
		diag2.getSymptoms().add(symptomRepo.findByName("Drhtavica"));
		diag2.getSymptoms().add(symptomRepo.findByName("Gubitak apetita"));
		diag2.getSymptoms().add(symptomRepo.findByName("Umor"));
		diag2.getSymptoms().add(symptomRepo.findByName("Zuti sekret iz nosa"));
		diag2.getTherapies().add(therapyRepo.findById((long) 3));
		diagRepo.save(diag2);
		
		Diagnosis diag3 = new Diagnosis();
		diag3.getIllnesses().add(illnessRepo.findByName("Dijabetes"));
		diag3.getSymptoms().add(symptomRepo.findByName("Cesto uriniranje"));
		diag3.getSymptoms().add(symptomRepo.findByName("Gubitak telesne tezine"));
		diag3.getSymptoms().add(symptomRepo.findByName("Zamor"));
		diag3.getSymptoms().add(symptomRepo.findByName("Mucnina i povracanje"));
		diag3.getTherapies().add(therapyRepo.findById((long) 2));
		diagRepo.save(diag3);
		
		PatientFile pf = new PatientFile();
		List<String> allergies = new ArrayList<String>();
		allergies.add("SastavA1");
		allergies.add("SastavC");
		allergies.add("SastavA2");
		allergies.add("SastavC1");
		pf.setAllergies(allergies);;
		pf.setAge(28);
		pf.setName("Margot");
		pf.setSurname("Robbie");
		pf.getDiagnosis().add(diagRepo.findById((long) 1));
		patientRepo.save(pf);
		
		PatientFile pf1 = new PatientFile();
		List<String> allergies1 = new ArrayList<String>();
		allergies1.add("SastavA1");
		allergies1.add("SastavB");
		allergies1.add("SastavB2");
		pf1.setAllergies(allergies1);;
		pf1.setAge(51);
		pf1.setName("Nicole");
		pf1.setSurname("Kidman");
		pf1.getDiagnosis().add(diagRepo.findById((long) 2));
		pf1.getDiagnosis().add(diagRepo.findById((long) 3));
		patientRepo.save(pf1);
		
		PatientFile pf2 = new PatientFile();
		List<String> allergies2 = new ArrayList<String>();
		allergies2.add("SastavA2");
		allergies2.add("SastavC1");
		pf2.setAllergies(allergies2);;
		pf2.setAge(62);
		pf2.setName("Tom");
		pf2.setSurname("Hanks");
		pf2.getDiagnosis().add(diagRepo.findById((long) 3));
		patientRepo.save(pf2);
		
		PatientFile pf3 = new PatientFile();
		List<String> allergies3 = new ArrayList<String>();
		allergies3.add("SastavA3");
		allergies3.add("SastavC2");
		pf3.setAllergies(allergies3);;
		pf3.setAge(50);
		pf3.setName("Will");
		pf3.setSurname("Smith");
		pf3.getDiagnosis().add(diagRepo.findById((long) 4));
		patientRepo.save(pf3);
	}

}
