package sistemi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sistemi.dto.IllnessDTO;
import sistemi.dto.SymptomDTO;
import sistemi.model.Illness;
import sistemi.model.Symptom;
import sistemi.repository.IllnessRepository;
import sistemi.repository.SymptomRepository;

@Service
public class SymptomService {
	
	@Autowired
	SymptomRepository symptomRepo;
	
	@Autowired
	IllnessRepository illnessRepo;

	public SymptomDTO getSymptom(Long id) {

		Symptom symptom = symptomRepo.findById(id);
		SymptomDTO symptomDTO = new SymptomDTO();
		symptomDTO.setName(symptom.getName());

		return symptomDTO;
	}
	
	public List<SymptomDTO> getSymptomsByIllness(String name) {
		
		List<SymptomDTO> symptomsDTO = new ArrayList<SymptomDTO>();
		if(name.equals("Sinusna infekcija")) {
			SymptomDTO symptom = new SymptomDTO();
			symptom.setName("*Pacijent bolovao od prehlade ili groznice u poslednjih 60 dana");
			symptomsDTO.add(symptom);
		}
		if(name.equals("Hronicna bubrezna bolest")) {
			SymptomDTO symptom = new SymptomDTO();
			symptom.setName("*Pacijent boluje od hipertenzije vise od 6 meseci");
			symptomsDTO.add(symptom);
			SymptomDTO symptom1 = new SymptomDTO();
			symptom1.setName("*Pacijent boluje od dijabetesa");
			symptomsDTO.add(symptom1);
		}
		if(name.equals("Akutna bubrezna povreda")) {
			SymptomDTO symptom = new SymptomDTO();
			symptom.setName("*Pacijent se oporavlja od operacije");
			symptomsDTO.add(symptom);
			SymptomDTO symptom1 = new SymptomDTO();
			symptom1.setName("*Pacijent imao povisenu telesnu temperaturu u poslednjih 14 dana");
			symptomsDTO.add(symptom1);
			SymptomDTO symptom2 = new SymptomDTO();
			symptom2.setName("*Pacijent primao antibiotike u poslednjih 21 dan");
			symptomsDTO.add(symptom2);
		}
		
		@SuppressWarnings("unchecked")
		List<Symptom> symptoms = symptomRepo.findAll();
		
		for(Symptom s : symptoms) {
			if(s.getIllnesses().contains(illnessRepo.findByName(name))) {
				symptomsDTO.add(new SymptomDTO(s));
			}
		}
		return symptomsDTO;
	}

	public List<SymptomDTO> getAll() {
		List<SymptomDTO> symptomsDTO = symptomRepo.findAll().stream()
				.map(symptom -> new SymptomDTO(symptom)).collect(Collectors.toList());
		return symptomsDTO;
	}

	public boolean create(SymptomDTO symptomDTO) {
		try {
			Symptom symptom = new Symptom();
			symptom.setName(symptomDTO.getName());
			
			symptomRepo.save(symptom);
			return true;
		}catch(Exception e) {
			
		}
		return false;
		
	}

	public boolean update(SymptomDTO symptomDTO, Long id) {
		try {
			Symptom symptom = symptomRepo.findById(id);
			symptom.setName(symptomDTO.getName());
			ArrayList<Illness> il = new ArrayList<Illness>();
			for(IllnessDTO i : symptomDTO.getIllnesses()) {
				Illness illness = illnessRepo.findByName(i.getName());
				il.add(illness);
			}
			symptom.setIllnesses(il.stream().collect(Collectors.toSet()));
			
			symptomRepo.save(symptom);
			return true;
		}catch(Exception e) {
			
		}
		return false;
		
	}

	public boolean delete(Long id) {
		try {
			symptomRepo.delete(symptomRepo.findById(id));
			return true;
		}catch(Exception e) {
			
		}
		return false;
		
	}

}
