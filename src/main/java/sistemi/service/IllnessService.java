package sistemi.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sistemi.dto.IllnessDTO;
import sistemi.dto.SymptomDTO;
import sistemi.model.Illness;
import sistemi.model.Symptom;
import sistemi.repository.IllnessRepository;
import sistemi.repository.SymptomRepository;

@Service
public class IllnessService {
	
	@Autowired
	private IllnessRepository illnessRepository;
	
	@Autowired
	private SymptomRepository symptomRepository;
	
	public IllnessDTO getIllness(String name) {
		
		Illness illness = illnessRepository.findByName(name);
		IllnessDTO illnessDTO = new IllnessDTO();
		List<Symptom> symptoms = illness.getSymptoms().stream().collect(Collectors.toList());
		illnessDTO.setName(illness.getName());
		illnessDTO.setId(illness.getId());
		for(Symptom s : symptoms) {
			illnessDTO.getSymptoms().add(new SymptomDTO(s));
		}
		return illnessDTO;
	}

	public boolean create(IllnessDTO illnessDTO) {
		Illness illness = new Illness();
		illness.setName(illnessDTO.getName());
		List<SymptomDTO> symptomsDTO = illnessDTO.getSymptoms();
		List<Symptom> symptoms = symptomRepository.findAll();
		for(SymptomDTO s : symptomsDTO) {
			for(Symptom s1 : symptoms) {
				if(s.getName().equals(s1.getName())) {
					illness.getSymptoms().add(s1);
				}
			}
		}
		illnessRepository.save(illness);
		return true;
	}

	public List<IllnessDTO> getAll() {
		List<IllnessDTO> illnessesDTO = illnessRepository.findAll().stream()
				.map(illness -> new IllnessDTO(illness)).collect(Collectors.toList());
		return illnessesDTO;
	}

	public List<IllnessDTO> delete(Long id) {
		illnessRepository.delete(id);
		return getAll();
	}

	

}
