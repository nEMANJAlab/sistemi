package sistemi.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sistemi.dto.MedicationDTO;
import sistemi.model.Medication;
import sistemi.repository.MedicationRepository;

@Service
public class MedicationService {
	
	@Autowired
	private MedicationRepository medicationRepo;
	
	
	private ModelMapper modelMapper;

	public List<MedicationDTO> getAll() {
		List<MedicationDTO> medicationsDTO = medicationRepo.findAll().stream()
				.map(medication -> new MedicationDTO(medication)).collect(Collectors.toList());
		return medicationsDTO;
	}

	public MedicationDTO getMedication(String name) {

		return new MedicationDTO(medicationRepo.findByName(name));
	}
	
	public boolean create(MedicationDTO medicationDTO) {
		Medication medication =  modelMapper.map(medicationDTO, Medication.class);
		medicationRepo.save(medication);
		
		return true;
	}

	

}
