package sistemi.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sistemi.dto.DiagnosisDTO;
import sistemi.dto.IllnessDTO;
import sistemi.dto.MedicationDTO;
import sistemi.dto.SymptomDTO;
import sistemi.model.Diagnosis;
import sistemi.model.Illness;
import sistemi.model.Medication;
import sistemi.model.PatientFile;
import sistemi.model.Symptom;
import sistemi.model.Therapy;
import sistemi.repository.DiagnosisRepository;
import sistemi.repository.IllnessRepository;
import sistemi.repository.MedicationRepository;
import sistemi.repository.PatientFileRepository;
import sistemi.repository.SymptomRepository;
import sistemi.repository.TherapyRepository;

@Service
public class DiagnosisService {
	
	private final KieContainer kieContainer;
	
	@Autowired
	public DiagnosisService(KieContainer kieContainer) {
		this.kieContainer = kieContainer;
	}
	
	@Autowired
	DiagnosisRepository diagRepo;
	
	@Autowired
	SymptomRepository symptomRepo;
	
	@Autowired
	IllnessRepository illnessRepo;
	
	@Autowired
	TherapyRepository therapyRepo;
	
	@Autowired
	MedicationRepository medicationRepo;

	@Autowired
	PatientFileRepository patientRepo;
	
	ModelMapper modelMapper;
	
	public DiagnosisDTO create(DiagnosisDTO diagnosisDTO) {
		
		KieSession kieSession = kieContainer.newKieSession("diagnosisSession");
		
		DiagnosisProcess diag = new DiagnosisProcess();
		Diagnosis diagnosis = new Diagnosis();
		List<SymptomDTO> symptomsDTO = diagnosisDTO.getSymptomsDTO();
		List<Symptom> symptoms = new ArrayList<Symptom>();
		List<Therapy> therapies = new ArrayList<Therapy>();
		Therapy therapy = new Therapy();
		PatientFile pf = patientRepo.findById(diagnosisDTO.getPatientId());
		
		for(SymptomDTO s : symptomsDTO) {
			if(s.getName().equals("Visok pritisak")) {
				if(checkForHipertension(pf, diag)) {
					diag.setHipertenzija(90.00);
				}else{diag.setHipertenzija(-1000);}
			}
			diag.getSymptoms().add(s.getName());
			Symptom symptom = symptomRepo.findByName(s.getName());
			symptoms.add(symptom);
			diagnosis.getSymptoms().add(symptom);
		}
		for(MedicationDTO m : diagnosisDTO.getMedicationsDTO()) {
			Medication med = medicationRepo.findByName(m.getName());
			therapy.getMedications().add(med);
		}
		
		therapies.add(therapy);
		diagnosis.getTherapies().add(therapy);
		
		kieSession.insert(diagnosis);
		kieSession.insert(diag);
		kieSession.fireAllRules();

		
		Illness illness = illnessRepo.findByName(diag.makeDiagnosis());
		
		if(illness.getName().equals("Sinusna infekcija") && checkForSinusna(pf, diag)) {
			diagnosis.getIllnesses().add(illness);
		}else {
			diag.setSinusnaInfekcija(-1000.00);
		}
		if(illness.getName().equals("Akutna bubrezna povreda") && checkForTemp(pf, diag) || illness.getName().equals("Akutna bubrezna povreda") && checkForAntibiotics(pf, diag)) {
			diagnosis.getIllnesses().add(illness);
		}else {
			diag.setAkutnaBubreznaPovreda(-1000.00);
		}
		if(illness.getName().equals("Hronicna bubrezna bolest") && checkForDiabetes(pf, diag) || illness.getName().equals("Hronicna bubrezna bolest") && checkForHiper(pf, diag)) {
			diagnosis.getIllnesses().add(illness);
		}else {
			diag.setHronicnaBubreznaBolest(-1000.00);
		}
		
		kieSession.insert(diagnosis);
		kieSession.insert(diag);
		kieSession.fireAllRules();
		kieSession.dispose();
		
		illness = illnessRepo.findByName(diag.makeDiagnosis());
		diagnosis.getIllnesses().add(illness);
		
		therapyRepo.save(therapy);
		diagRepo.save(diagnosis);
		pf.getDiagnosis().add(diagnosis);
		patientRepo.save(pf);
		return new DiagnosisDTO(diagnosis);
	}
	
	private boolean checkForHipertension(PatientFile file, DiagnosisProcess diag) {
		int Hipertension = 0;
		for(Diagnosis d : file.getDiagnosis()) {
				for(Symptom s : d.getSymptoms()) {
					if(s.getName().equals("Visok pritisak")) {
						Hipertension += 1;			
				}
			}
		}
		if(Hipertension > 9)
			return true;
		return false;
	}
	
	private boolean checkForTemp(PatientFile file, DiagnosisProcess diag) {
		for(Diagnosis d : file.getDiagnosis()) {
				for(Symptom s : d.getSymptoms()) {
					if(s.getName().equals("Temperatura veca od 38") || s.getName().equals("Temperatura od 40 do 41")) {
						diag.setAkutnaBubreznaPovreda(28.00);
						return true;
				}
			}
		}
		return false;
	}

	public boolean checkForSinusna(PatientFile file, DiagnosisProcess diag) {
		for(Diagnosis d : file.getDiagnosis()) {
			for(Illness i : d.getIllnesses()) {
				if(i.getName().equals("Groznica") || i.getName().equals("Prehlada")) {
					diag.setSinusnaInfekcija(14.5);
					if(diag.getSinusnaInfekcija() == 100.00)
						diag.setSinusnaInfekcija(5);
					return true;
				}	
			}
		}
		return false;
	}
	
	public boolean checkForDiabetes(PatientFile file, DiagnosisProcess diag) {
		for(Diagnosis d : file.getDiagnosis()) {
			for(Illness i : d.getIllnesses()) {
				if(i.getName().equals("Dijabetes")) {
					diag.setHronicnaBubreznaBolest(20.00);
					return true;
				}	
			}
		}
		return false;
	}
	
	public boolean checkForHiper(PatientFile file, DiagnosisProcess diag) {
		for(Diagnosis d : file.getDiagnosis()) {
			for(Illness i : d.getIllnesses()) {
				if(i.getName().equals("Hipertenzija")) {
					diag.setHronicnaBubreznaBolest(20.00);
					return true;
				}	
			}
		}
		return false;
	}
	
	public boolean checkForAntibiotics(PatientFile file, DiagnosisProcess diag) {
		for(Diagnosis d : file.getDiagnosis()) {
			for(Therapy t : d.getTherapies()) {
				for(Medication m : t.getMedications())
				if(m.getType().equals("antibiotik")) {
					diag.setAkutnaBubreznaPovreda(28.00);
					return true;
				}	
			}
		}
		return false;
	}

	public List<DiagnosisDTO> getAll() {
		List<DiagnosisDTO> diagnosisDTO = diagRepo.findAll().stream().map(diag -> new DiagnosisDTO(diag)).collect(Collectors.toList());
		return diagnosisDTO;
	}

	public List<DiagnosisDTO> delete(Long id) {
		Diagnosis diag = diagRepo.findById(id);
		Illness il = new Illness();
		il.setName("None");
		illnessRepo.save(il);
		Set<Illness> newIllnesses = new HashSet<Illness>();
		newIllnesses.add(il);
		diag.setIllnesses(newIllnesses);
		diagRepo.save(diag);
		return null;
	}

	
	

}
