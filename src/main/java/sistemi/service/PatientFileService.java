package sistemi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import sistemi.dto.DiagnosisDTO;
import sistemi.dto.IllnessDTO;
import sistemi.dto.PatientFileDTO;
import sistemi.model.Diagnosis;
import sistemi.model.Illness;
import sistemi.model.Medication;
import sistemi.model.PatientFile;
import sistemi.model.Therapy;
import sistemi.repository.PatientFileRepository;

@Service
public class PatientFileService {
	
	@Autowired
	PatientFileRepository patientRepo;
	
	@Bean
	public ModelMapper modelMapper() {
	    return new ModelMapper();
	}
	
	@Autowired
    private ModelMapper modelMapper;

	public List<PatientFileDTO> getAll() {
		List<PatientFileDTO> patientsDTO = patientRepo.findAll().stream()
				.map(patient -> new PatientFileDTO(patient)).collect(Collectors.toList());
				
		return patientsDTO;
	}

	public PatientFileDTO getPatient(Long id) {
		PatientFileDTO patientDTO = new PatientFileDTO(patientRepo.findById(id));
		return patientDTO;
	}

	public PatientFileDTO create(PatientFileDTO patientDTO) {
		PatientFile patient = modelMapper.map(patientDTO, PatientFile.class);
		patientRepo.save(patient);
		System.out.println(patient.getName() +  patient.getSurname() + patient.getAge());
		return new PatientFileDTO(patient);
	}

	public List<PatientFileDTO> delete(Long id) {
		patientRepo.delete(id);
		return getAll();
	}


	public List<PatientFileDTO> getHronic() {
		List<PatientFile> allPatients = patientRepo.findAll();
		List<PatientFileDTO> p = new ArrayList<PatientFileDTO>();
		
		for(PatientFile patientFile : allPatients) {
			PatientFileDTO patientDTO = new PatientFileDTO();
			patientDTO.setName(patientFile.getName());
			patientDTO.setSurname(patientFile.getSurname());
			DiagnosisDTO diagnosisDTO = new DiagnosisDTO();
			int upalaKrajnika = 0;
			int sinusnaInfekcija = 0;
			int hronicnaBubreznaBolest = 0;
			int akutnaBubreznaPovreda = 0;
			int hipertenzija = 0;
			int dijabetes = 0;
			for (Diagnosis diagnosis : patientFile.getDiagnosis()) {
				for(Illness illness: diagnosis.getIllnesses()) {
					if(illness.getName().equals("Upala krajnika")) {
						upalaKrajnika +=1;
						if(upalaKrajnika == 6) {
							diagnosisDTO.getIllnessesDTO().add(new IllnessDTO(illness));
							patientDTO.getDiagnosisDTO().add(diagnosisDTO);
						}
					}
						
					if(illness.getName().equals("Sinusna infekcija")) {
						sinusnaInfekcija +=1;
						if(sinusnaInfekcija == 6) {
							diagnosisDTO.getIllnessesDTO().add(new IllnessDTO(illness));
							patientDTO.getDiagnosisDTO().add(diagnosisDTO);
						}
					}
						
					if(illness.getName().equals("Hronicna bubrezna bolest")) {
						hronicnaBubreznaBolest +=1;
						if(hronicnaBubreznaBolest == 6) {
							diagnosisDTO.getIllnessesDTO().add(new IllnessDTO(illness));
							patientDTO.getDiagnosisDTO().add(diagnosisDTO);
						}
					}
						
					if(illness.getName().equals("Akutna bubrezna povreda")) {
						akutnaBubreznaPovreda +=1;
						if(akutnaBubreznaPovreda == 6) {
							diagnosisDTO.getIllnessesDTO().add(new IllnessDTO(illness));
							patientDTO.getDiagnosisDTO().add(diagnosisDTO);
						}
					}
						
					if(illness.getName().equals("Hipertenzija")) {
						hipertenzija +=1;
						if(hipertenzija == 6) {
							diagnosisDTO.getIllnessesDTO().add(new IllnessDTO(illness));
							patientDTO.getDiagnosisDTO().add(diagnosisDTO);
						}
					}
						
					if(illness.getName().equals("Dijabetes")) {
						dijabetes +=1;
						if(dijabetes == 6) {
							//report.put(illness.getName(), patientFile.getName() + " " + patientFile.getSurname());
							//report.put(patientFile, illness);
							diagnosisDTO.getIllnessesDTO().add(new IllnessDTO(illness));
							patientDTO.getDiagnosisDTO().add(diagnosisDTO);
						}
					}
						
				}
				
			}
			if(patientDTO.getDiagnosisDTO().size() > 0) {
				p.add(patientDTO);
			}
		}
		
		return p;
	}
	
	public List<PatientFileDTO> getAddiction() {
		List<PatientFile> allPatients = patientRepo.findAll();
		List<PatientFileDTO> p = new ArrayList<PatientFileDTO>();
		
		for(PatientFile patientFile : allPatients) {
			PatientFileDTO patientDTO = new PatientFileDTO();
			patientDTO.setName(patientFile.getName());
			patientDTO.setSurname(patientFile.getSurname());
			int analgetik = 0;
			
			for (Diagnosis diagnosis : patientFile.getDiagnosis()) {
				for(Therapy therapy: diagnosis.getTherapies()) {
					for(Medication medication : therapy.getMedications()) {
						if(medication.getType().equals("analgetik"))
							analgetik += 1;
						if(analgetik == 6) {
							p.add(patientDTO);
						}
					}
				}
				
			}
			
		}
		
		return p;
	}
}
