package sistemi.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class DiagnosisProcess {
	
	private List<String> symptoms;
	private List<String> illnesses;
	private double prehlada = 0.0;
	private double groznica = 0.0;
	private double sinusnaInfekcija = 0.0;
	private double upalaKrajnika = 0.0;
	private double dijabetes = 0.0;
	private double hipertenzija = 0.0;
	private double akutnaBubreznaPovreda = 0.0;
	private double hronicnaBubreznaBolest = 0.0;
	
	public DiagnosisProcess() {
		this.symptoms = new ArrayList<String>();
		this.illnesses = new ArrayList<String>();
	}
	
	public String makeDiagnosis() {
		
		String illness = "None";
		HashMap<String, Integer> mapa = new HashMap<String, Integer>();
		mapa.put("Prehlada", (int)getPrehlada());
		mapa.put("Groznica", (int)getGroznica());
		mapa.put("Sinusna infekcija", (int)getSinusnaInfekcija());
		mapa.put("Upala krajnika", (int)getUpalaKrajnika());
		mapa.put("Dijabetes", (int)getDijabetes());
		mapa.put("Hipertenzija", (int)getHipertenzija());
		mapa.put("Akutna bubrezna povreda", (int)getAkutnaBubreznaPovreda());
		mapa.put("Hronicna bubrezna bolest", (int)getHronicnaBubreznaBolest());
		int maxVal = (Collections.max(mapa.entrySet(), (entry1, entry2) -> entry1.getValue() - entry2.getValue()).getValue());
		
		for (Map.Entry<String, Integer> entry : mapa.entrySet()) {
		    if(entry.getValue() == maxVal && maxVal >= 70) {
		    	illness = entry.getKey();
		    	
		    }
		    if(entry.getValue() == 99.75) {
	    		System.out.println(entry.getKey() + " EEEEEE");
	    	}
		}

		
		
		/*if(getPrehlada() <= getGroznica() && getGroznica() >= 57.0) {
			illness = "Groznica";
			if(getGroznica() <= getSinusnaInfekcija())
				illness = "Sinusna infekcija";
			if(getUpalaKrajnika() >= getSinusnaInfekcija())
				illness = "Upala Krajnika";
			if(getUpalaKrajnika() <= getDijabetes())
				illness = "Dijabetes";
			if(getDijabetes() <= getHipertenzija())
				illness = "Hipertenzija";
			if(getHipertenzija() <= getAkutnaBubreznaPovreda())
				illness = "Akutna bubrezna povreda";
			if(getAkutnaBubreznaPovreda() <= getHronicnaBubreznaBolest())
				illness = "Hronicna bubrezna bolest";
		}else if(getPrehlada() >= getGroznica() && getPrehlada() >= 57.0) {
			illness = "Prehlada";
			if(getPrehlada() <= getSinusnaInfekcija())
				illness = "Sinusna infekcija";
			if(getUpalaKrajnika() >= getSinusnaInfekcija())
				illness = "Upala Krajnika";
			if(getUpalaKrajnika() <= getDijabetes())
				illness = "Dijabetes";
			if(getDijabetes() <= getHipertenzija())
				illness = "Hipertenzija";
			if(getHipertenzija() <= getAkutnaBubreznaPovreda())
				illness = "Akutna bubrezna povreda";
			if(getAkutnaBubreznaPovreda() <= getHronicnaBubreznaBolest())
				illness = "Hronicna bubrezna bolest";
		}*/
			return illness;
	}

	public List<String> getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(List<String> syptoms) {
		this.symptoms = syptoms;
	}

	public List<String> getIllnesses() {
		return illnesses;
	}

	public void setIllnesses(String illness) {
		this.illnesses.add(illness);
	}

	public double getPrehlada() {
		return prehlada;
	}

	public void setPrehlada(double prehlada) {
		this.prehlada += prehlada;
	}

	public double getGroznica() {
		return groznica;
	}

	public void setGroznica(double groznica) {
		this.groznica += groznica;
	}

	public double getSinusnaInfekcija() {
		return sinusnaInfekcija;
	}

	public void setSinusnaInfekcija(double sinusnaInfekcija) {
		this.sinusnaInfekcija += sinusnaInfekcija;
	}

	public double getUpalaKrajnika() {
		return upalaKrajnika;
	}

	public void setUpalaKrajnika(double upalaKrajnika) {
		this.upalaKrajnika += upalaKrajnika;
	}

	public double getDijabetes() {
		return dijabetes;
	}

	public void setDijabetes(double dijabetes) {
		this.dijabetes += dijabetes;
	}

	public double getHipertenzija() {
		return hipertenzija;
	}

	public void setHipertenzija(double hipertenzija) {
		this.hipertenzija += hipertenzija;
	}

	public double getAkutnaBubreznaPovreda() {
		return akutnaBubreznaPovreda;
	}

	public void setAkutnaBubreznaPovreda(double akutnaBubreznaPovreda) {
		this.akutnaBubreznaPovreda += akutnaBubreznaPovreda;
	}

	public double getHronicnaBubreznaBolest() {
		return hronicnaBubreznaBolest;
	}

	public void setHronicnaBubreznaBolest(double hronicnaBubreznaBolest) {
		this.hronicnaBubreznaBolest += hronicnaBubreznaBolest;
	}
	
	

}
