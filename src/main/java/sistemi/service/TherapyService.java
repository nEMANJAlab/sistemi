package sistemi.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sistemi.dto.TherapyDTO;
import sistemi.model.Therapy;
import sistemi.repository.TherapyRepository;

@Service
public class TherapyService {
	
	@Autowired
	private TherapyRepository therapyRepo;
	
	@Autowired
	private ModelMapper modelMapper;

	public List<TherapyDTO> getAll() {
		List<TherapyDTO> therapiesDTO = therapyRepo.findAll().stream()
				.map(therapy -> new TherapyDTO(therapy)).collect(Collectors.toList());
		
		return therapiesDTO;
	}

	public TherapyDTO getTherapy(Long id) {
		return new TherapyDTO(therapyRepo.findById(id));
	}

	public boolean create(TherapyDTO therapyDTO) {
		Therapy therapy =  modelMapper.map(therapyDTO, Therapy.class);
		therapyRepo.save(therapy);
		return true;
	}

}
