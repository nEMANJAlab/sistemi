package sistemi.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name="illnesses")
public class Illness {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToMany(cascade= {CascadeType.REMOVE})
	@JoinTable(name = "illness_symptom", joinColumns = @JoinColumn(name = "illness_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "symptom_id", referencedColumnName = "id"))
	private Set<Symptom> symptoms = new HashSet<Symptom>();
	
	@ManyToMany(mappedBy="illnesses", cascade= {CascadeType.REMOVE})
	private Set<Diagnosis> diagnosis;

	public Set<Symptom> getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(Set<Symptom> symptoms) {
		this.symptoms = symptoms;
	}

	public Set<Diagnosis> getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(Set<Diagnosis> diagnosis) {
		this.diagnosis = diagnosis;
	}
	
	
	
}
