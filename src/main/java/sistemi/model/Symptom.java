package sistemi.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="symptoms")
public class Symptom {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	@ManyToMany(mappedBy="symptoms")
	private Set<Illness> illnesses;
	
	@ManyToMany(mappedBy="symptoms", cascade ={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	private Set<Diagnosis> diagnosis;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Illness> getIllnesses() {
		return illnesses;
	}

	public void setIllnesses(Set<Illness> illnesses) {
		this.illnesses = illnesses;
	}

	public Set<Diagnosis> getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(Set<Diagnosis> diagnosis) {
		this.diagnosis = diagnosis;
	}
	
	
	
}
