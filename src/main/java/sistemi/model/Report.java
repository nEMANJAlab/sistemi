package sistemi.model;

import java.util.List;

public class Report {
	
	private PatientFile patient;
	private List<Illness> illnesses;
	public PatientFile getPatient() {
		return patient;
	}
	public void setPatient(PatientFile patient) {
		this.patient = patient;
	}
	public List<Illness> getIllnesses() {
		return illnesses;
	}
	public void setIllnesses(List<Illness> illnesses) {
		this.illnesses = illnesses;
	}
	
	

}
