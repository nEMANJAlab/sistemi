package sistemi.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="diagnosis")
public class Diagnosis {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToMany()
	@JoinTable(name = "diagnosis_symptom", joinColumns = @JoinColumn(name = "diagnosis_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "symptom_id", referencedColumnName = "id"))
	private Set<Symptom> symptoms = new HashSet<Symptom>();
	
	@ManyToMany(cascade= {CascadeType.REMOVE})
	@JoinTable(name = "diagnosis_illness", joinColumns = @JoinColumn(name = "diagnosis_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "illness_id", referencedColumnName = "id"))
	private Set<Illness> illnesses = new HashSet<Illness>();
	
	@ManyToMany(mappedBy="diagnosis", cascade= {CascadeType.REMOVE})
	private Set<PatientFile> patients = new HashSet<PatientFile>();
	
	@ManyToMany(cascade= {CascadeType.REMOVE})
	@JoinTable(name = "diagnosis_therapy", joinColumns = @JoinColumn(name = "diagnosis_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "therapy_id", referencedColumnName = "id"))
	private Set<Therapy> therapies = new HashSet<Therapy>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Symptom> getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(Set<Symptom> symptoms) {
		this.symptoms = symptoms;
	}

	public Set<Illness> getIllnesses() {
		return illnesses;
	}

	public void setIllnesses(Set<Illness> illnesses) {
		this.illnesses = illnesses;
	}

	public Set<PatientFile> getPatients() {
		return patients;
	}

	public void setPatients(Set<PatientFile> patients) {
		this.patients = patients;
	}

	public Set<Therapy> getTherapies() {
		return therapies;
	}

	public void setTherapies(Set<Therapy> therapies) {
		this.therapies = therapies;
	}

	
	
	
}
